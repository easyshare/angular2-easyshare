import { EsharePage } from './app.po';

describe('eshare App', () => {
  let page: EsharePage;

  beforeEach(() => {
    page = new EsharePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
