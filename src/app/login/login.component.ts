import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { DataService } from '../shared/data.service';
import { UserService } from '../shared/user.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnChanges {
    @Input() popup: boolean;
    loginModalVisible = false;
    loginName = '';
    loginPassWord = '';

    constructor(private dataService: DataService, private userService: UserService) { }

    ngOnChanges(changes: any) {
        if (changes.popup.currentValue === true) { this.loginModalVisible = true; }
    }

    onCancelSubmit() {
        this.loginModalVisible = false;
    }

    onLoginSubmit() {
        this.dataService.login(this.loginName, this.loginPassWord).subscribe
            (data => {
                if (data.Loggedin) {
                    this.userService.setUser(this.loginName, this.loginPassWord, data.UserType);
                    window.location.href = '/';
                    this.loginModalVisible = false;
                } else {
                    alert('Wrong userid or password - please try again');
                }
                // console.log('nieuwe user:', this.userService);
            });
        this.loginModalVisible = false;
    }

    ngOnInit() {
    }

}
