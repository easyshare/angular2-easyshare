import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppAuthService } from '../shared/app-auth.service';
import { WatchlistResolveService } from './watchlist-resolve.service';
import { WatchlistAuthService } from './watchlist-auth.service';
import { WatchlistComponent } from './watchlist.component';
import { ScreenerComponent } from './screener/screener.component';
import { ScreenerResolveService } from './screener/screener-resolve.service';
import { ScreenerAuthService } from './screener/screener-auth.service';
import { UsersComponent } from '../users/users.component';
import { UsersResolveService } from '../users/users-resolve.service';

const routes: Routes = [
    {
        path: 'watchlist',
        component: WatchlistComponent,
        canActivate: [AppAuthService, WatchlistAuthService],
        resolve: { stocks: WatchlistResolveService }
    },
    {
        path: 'watchlist/:list',
        component: WatchlistComponent,
        canActivate: [AppAuthService, WatchlistAuthService],
        resolve: { stocks: WatchlistResolveService }
    },
    {
        path: 'screener',
        component: ScreenerComponent,
        canActivate: [AppAuthService, ScreenerAuthService],
        resolve: { stocks: ScreenerResolveService }
    },
    {
        path: 'screener/:parameters',
        component: ScreenerComponent,
        canActivate: [AppAuthService, ScreenerAuthService],
        resolve: { stocks: ScreenerResolveService }
    },
    {
        path: 'users',
        canActivate: [AppAuthService],
        component: UsersComponent,
        resolve: { users: UsersResolveService }
    },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: []
})
export class WatchlistRoutingModule { }
