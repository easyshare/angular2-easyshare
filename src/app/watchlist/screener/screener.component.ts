import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { UserService } from '../../shared/user.service';
import { FunctionsService } from '../../shared/functions.service';
import { Router, ActivatedRoute } from '@angular/router';
import { WatchlistGridComponent } from '../watchlist-grid/watchlist-grid.component';

@Component({
    selector: 'app-screener',
    templateUrl: './screener.component.html',
    styleUrls: ['./screener.component.css']
})
export class ScreenerComponent implements OnInit {
    @ViewChild(WatchlistGridComponent)
    grid: WatchlistGridComponent;
    filters: any;
    selectedFilter: string;
    screenerlist = '';
    parameters = [];
    parametervalues = [];
    stocks: any = [];
    infoToShow: string;
    infoVisible = false;

    constructor(public user: UserService, private router: Router,
        private route: ActivatedRoute, private f: FunctionsService, private myElement: ElementRef) {
        this.filters = [
            { value: 'screpsgst_5yc', Filter: 'Continuous growth' },
            { value: 'screpsgst_5yc-scrroe_g10-scrpeg_s15-scrder_s33-screpsgp_g15', Filter: "Ivo's top picks" },
            { value: 'scrpbr_s1', Filter: ' Low Price book value' },
            { value: 'scrroe_g10-scrder_s33-scrsma20_pa-scrsma50_pa-scrsma200_pa-screpsgp_g10-scrsalgp_g10-scrprm_g10', Filter: 'Toppers'},
            { value: 'scrdiv_Growing', Filter: 'Dividend groeiers' }
        ];
        this.selectedFilter = 'Choose ...';
    }

    ngOnInit() {
        console.log('screener - init');
        this.route.data.forEach((data: { stocks: any[] }) => {
            for (let stock of data.stocks) {
                stock = this.f.calculateStock(stock);
            }
            this.user.stocks = data.stocks;
            this.stocks = data.stocks;
        });
        // is het een specifieke screenerlist
        for (const f of this.filters){
            if (f.value === this.user.screenerList) { this.selectedFilter = f.Filter; }
        }
        this.user.curStockList = 'screener';
        const hulp = this.user.screenerList.split('-');
        // this.resetParameters();
        if (hulp[0] === '') { hulp.length = 0; }
        for (let index = 0; index < hulp.length; index++) {
            const hulp1 = hulp[index].split('_');
            const value = (hulp1.length > 0) ? hulp1[1] : '';
            this.changeSelectValue(hulp1[0], value);
        }
    }

    // ************************** events **************************

    onParChange(selectElement) {
        const par = selectElement.id;
        const waarde = selectElement.value;
        this.changeSelectValue(par, waarde);
        this.router.navigate(['screener', this.user.screenerList]);
    }


    OnResetClick() {
        this.resetParameters();
        this.router.navigate(['screener', this.user.screenerList]);
    }

    onPresetClick(filter) {
        this.resetParameters();
        this.selectedFilter = filter.Filter;
        const list = filter.value;
        if (list !== '') {
            const hulp = list.split('-');
            for (let index = 0; index < hulp.length; index++) {
                const hulp1 = hulp[index].split('_');
                const value = (hulp1.length > 0) ? hulp1[1] : '';
                this.changeSelectValue(hulp1[0], value);
            }
            this.router.navigate(['screener', this.user.screenerList]);
        }
    }

    onViewClick(event) {
        this.grid.setView(event.target.textContent);
    }

    // ********************** procedures ***************************************

    changeSelectValue(par, waarde) {
        const selectElement = this.myElement.nativeElement.querySelector('#'.concat(par));
        const pl = this.parameters.indexOf(par);
        if (waarde === '') {
            if (pl > -1) {
                this.parameters.splice(pl, 1);
                this.parametervalues.splice(pl, 1);
                selectElement.classList.remove('selectselected');
            }
        } else {
            if (pl === -1) {
                selectElement.classList.add('selectselected');
                selectElement.value = waarde;
                this.parameters.push(par);
                this.parametervalues.push(waarde);
            } else {
                this.parametervalues[pl] = waarde;
            }
        }
        this.screenerlist = '';
        for (let index = 0; index < this.parameters.length; ++index) {
            this.screenerlist = this.screenerlist.concat(
                this.parameters[index],
                '_',
                this.parametervalues[index],
                ((index + 1) < this.parameters.length) ? '-' : '');
        }
        this.user.setScreenerList(this.screenerlist);
    }

    resetParameters() {
        for (let index = 0; index < this.parameters.length; ++index) {
            const id = this.parameters[index];
            const element = this.myElement.nativeElement.querySelector('#'.concat(id));
            element.classList.remove('selectselected');
            if (element.classList.contains('screener-combo-text')) {
                element.options[0].selected = true;
            }
            if (element.classList.contains('screener-input-text')) {
                element.value = '';
            }
        }
        this.screenerlist = '';
        this.user.setScreenerList(this.screenerlist);
        this.parameters = [];
        this.parametervalues = [];
        this.selectedFilter = 'Choose ...';
    }

    onShowInfo(columnName) {
        this.infoToShow = columnName;
        this.infoVisible = true;
    }

}
