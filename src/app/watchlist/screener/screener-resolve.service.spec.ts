import { TestBed, inject } from '@angular/core/testing';

import { ScreenerResolveService } from './screener-resolve.service';

describe('ScreenerResolveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ScreenerResolveService]
    });
  });

  it('should ...', inject([ScreenerResolveService], (service: ScreenerResolveService) => {
    expect(service).toBeTruthy();
  }));
});
