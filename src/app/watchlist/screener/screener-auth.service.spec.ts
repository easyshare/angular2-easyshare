import { TestBed, inject } from '@angular/core/testing';

import { ScreenerAuthService } from './screener-auth.service';

describe('ScreenerAuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ScreenerAuthService]
    });
  });

  it('should ...', inject([ScreenerAuthService], (service: ScreenerAuthService) => {
    expect(service).toBeTruthy();
  }));
});
