import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from '../../shared/user.service';

@Injectable()
export class ScreenerAuthService implements CanActivate {

    constructor(private user: UserService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        // ****************************************************************
        //    Zet de parameters van de route in orde
        // ****************************************************************
        let list = route.params['parameters'];
        if (list === undefined) { // er staat geen list in de url
            list = this.user.screenerList;
        }
        console.log('screenerlist set to ', list);
        return true;

    }
}
