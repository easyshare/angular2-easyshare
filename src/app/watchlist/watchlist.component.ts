import { Component, OnInit, ViewChild, OnDestroy, HostListener } from '@angular/core';
import { UserService } from '../shared/user.service';
import { DataService } from '../shared/data.service';
import { FunctionsService } from '../shared/functions.service';
import { Filter } from '../shared/filter';
import { ActivatedRoute, Router } from '@angular/router';
import { WatchlistGridComponent } from './watchlist-grid/watchlist-grid.component';
import { WatchlistEditComponent } from './watchlist-edit/watchlist-edit.component';

@Component({
    selector: 'app-watchlist',
    templateUrl: './watchlist.component.html',
    styleUrls: ['./watchlist.component.css'],
})

export class WatchlistComponent implements OnInit {
    @ViewChild(WatchlistGridComponent)
    grid: WatchlistGridComponent;
    @ViewChild(WatchlistEditComponent)
    listEdit: WatchlistEditComponent;

    filters = this.user.filters;
    stocks: any;
    myRefresh: number;
    refreshtime = 300000;

    constructor(public user: UserService, private router: Router,
        private route: ActivatedRoute, private f: FunctionsService, private dataService: DataService) { }

    @HostListener('window:focus', ['$event'])
    handleFocus(event: Event) { this.refresh(); }

    ngOnInit() {
        console.log('watchlist - component onInit');
        this.route.data.forEach((data: { stocks: any[] }) => {
            for (let stock of data.stocks) {
                stock = this.f.calculateStock(stock);
            }
            this.user.stocks = data.stocks;
            this.stocks = data.stocks;
        });
        this.user.curStockList = this.user.selectedFilter.Filter;
    }

    refresh() {
        this.dataService.loadstocklist(this.user.selectedFilter.Query, this.user.selectedFilter.Stocks, this.user.name)
            .subscribe(data => {
                for (let stock of data) {
                    stock = this.f.calculateStock(stock);
                }
                this.user.stocks = data;
                this.stocks = data;
            });

        console.log('grid refreshed');
    }

    // ****************** events *********************

    onFilterClick(filter) {
        this.user.curStockList = filter.Filter;
        this.router.navigate(['watchlist', filter.FilterUrl]);
    }

    onViewClick(event) {
        this.grid.setView(event.target.textContent);
    }

    onEditClick(nieuw: boolean) {
        this.listEdit.show(nieuw);
    }

    onFilterChanged(message: string) {
        console.log('grid filter changed');
        this.refresh();
    }

}





