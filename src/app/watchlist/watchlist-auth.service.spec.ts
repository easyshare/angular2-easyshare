/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { WatchlistAuthService } from './watchlist-auth.service';

describe('WatchlistAuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WatchlistAuthService]
    });
  });

  it('should ...', inject([WatchlistAuthService], (service: WatchlistAuthService) => {
    expect(service).toBeTruthy();
  }));
});
