import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from '../shared/user.service';

@Injectable()
export class WatchlistAuthService implements CanActivate {

  constructor(private user: UserService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // ****************************************************************
    //    Zet de parameters van de route in orde
    // ****************************************************************
    let list = route.params['list'];
    if (list === undefined) { // er staat geen list in de url
      list = this.user.selectedFilter.FilterUrl;
      this.router.navigate([state.url, list]);
      return false;
    } else {
      list = list.replace(/_/g, ' ');
      this.user.setFilter(list); // indien list niet bestaat wordt de eerste in de lijst genomen
      console.log('watchlist set to ', this.user.filterNaam);
      return true;
    }
  }
}
