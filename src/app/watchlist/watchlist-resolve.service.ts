import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { UserService} from '../shared/user.service';
import { DataService} from '../shared/data.service';
import { Observable } from 'rxjs';

@Injectable()
export class WatchlistResolveService implements Resolve<any> {

    constructor( private user: UserService, private dataService: DataService, private router: Router) { }

    resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | any {
          // console.log("in de resolve",route);
          return  this.dataService.loadstocklist(this.user.selectedFilter.Query, this.user.selectedFilter.Stocks, this.user.name);
    }
}
