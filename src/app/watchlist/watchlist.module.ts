import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

import { WatchlistRoutingModule } from './watchlist-routing.module';
import { WatchlistComponent } from './watchlist.component';
import { WatchlistGridComponent } from './watchlist-grid/watchlist-grid.component';
import { ScreenerComponent } from './screener/screener.component';

import { WatchlistResolveService } from './watchlist-resolve.service';
import { WatchlistAuthService } from './watchlist-auth.service';
import { ScreenerResolveService } from './screener/screener-resolve.service';
import { ScreenerAuthService } from './screener/screener-auth.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { WatchlistEditComponent } from './watchlist-edit/watchlist-edit.component';

@NgModule({
  imports: [
    CommonModule,
    WatchlistRoutingModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  declarations: [
    WatchlistComponent,
    ScreenerComponent,
    WatchlistGridComponent,
    WatchlistEditComponent,
  ],
   providers: [WatchlistResolveService, WatchlistAuthService, ScreenerResolveService, ScreenerAuthService],
})
export class WatchlistModule { }
