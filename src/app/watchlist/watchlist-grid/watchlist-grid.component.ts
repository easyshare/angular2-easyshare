import { Component, OnInit, OnDestroy, Input, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../shared/user.service';
import { DataService } from '../../shared/data.service';
import { OrderbyPipe } from '../../shared/orderby.pipe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../../shared/shared.module';
import { FourTradersModalComponent } from '../../shared/fourtraders-modal/fourtraders-modal.component';

@Component({
    selector: 'app-watchlist-grid',
    templateUrl: './watchlist-grid.component.html',
    styleUrls: ['./watchlist-grid.component.css'],
})

export class WatchlistGridComponent implements OnInit , OnDestroy {
    @Input() stocks: Array<any>;
    @ViewChild(FourTradersModalComponent) fourTraders: FourTradersModalComponent;

    myColDefs = [];
    ColDefs = [
        { name: 'Company', index: 'Company', formatter: 'text', cellStyle: {'font-weight': 'bold' }, info: false },
        { name: 'Name', index: 'Name', formatter: 'text' , info: false },
        { name: 'Date', index: 'RateDate', formatter: 'text', info: false  },
        { name: 'Value', index: 'Value', formatter: 'text', cellStyle: { 'text-align': 'right', 'font-weight': 'bold' }, info: false  },
        { name: 'Change', index: 'Difference', formatter: 'text', cellStyle: { 'text-align': 'right' } , info: false },
        { name: 'Perc.', index: 'Percent', formatter: 'myboldperc', cellStyle: { 'text-align': 'right', 'font-weight': 'bold' }
        , info: false  },
        { name: 'Industry', index: 'Industry', formatter: 'text', info: true },
        { name: 'Trend', index: 'Kml', formatter: 'myTrend' ,  info: true},
        { name: 'Macd', index: 'Macd', formatter: 'myboldperc', cellStyle: { 'text-align': 'right' }, info: true}
    ];
    colDefsGenl = [
        { name: 'Perf5Y', index: 'Perf5Y', formatter: 'text', cellStyle: { 'text-align': 'right' }, info: false },
        { name: 'Perf3Y', index: 'Perf3Y', formatter: 'text', cellStyle: { 'text-align': 'right' }, info: false },
        { name: 'Perf1Y', index: 'Perf1Y', formatter: 'text', cellStyle: { 'text-align': 'right' }, info: false },
        { name: 'Perf6M', index: 'Perf6M', formatter: 'text', cellStyle: { 'text-align': 'right' }, info: false },
        { name: 'Perf1M', index: 'Perf1M', formatter: 'text', cellStyle: { 'text-align': 'right' }, info: false }
    ];
    colDefsNaic = [
        { name: 'Stars', index: 'Stars', formatter: 'mystars', cellStyle: { 'text-align': 'right' }, info: true },
        { name: 'Rating', index: 'Rating', formatter: 'text', cellStyle: { 'text-align': 'right' }, info: false},
        { name: 'OmzetGroei', index: 'OmzetGroei', formatter: 'myPercent', cellStyle: { 'text-align': 'right'}, info: false },
        { name: 'WpaGroei', index: 'WpaGroei', formatter: 'myPercent', cellStyle: { 'text-align': 'right'}, info: false},
        { name: 'Advice', index: 'Advice', formatter: 'text', cellStyle: { 'text-align': 'right', 'font-weight': 'bold' }
        , info: true},
        { name: 'DivReturn', index: 'DivReturn', formatter: 'text', cellStyle: { 'text-align': 'right'}, info: false },
        { name: 'TotReturn', index: 'TotReturn', formatter: 'text', cellStyle: { 'text-align': 'right'}, info: false },
        { name: '4Tr.', index: '4TradersImg', formatter: 'my4Traders', cellStyle: { 'text-align': 'right'}, info: true }
    ];
    ColDefsMob = [
        { name: 'Company', index: 'Company', formatter: 'companyname' },
        { name: 'Date', index: 'RateDate', formatter: 'datevalue' },
    ];

    view = '';
    infoToShow;
    infoVisible = false;

    permitted = 10000;
    truncated = false;

    // interval = 5000;
    // retrys = 8;
    // stocksToUpdate = '';


    constructor(private user: UserService, private dataService: DataService, private router: Router) { }

    ngOnInit() {
        console.log('watchlist grid - component onInit');
        if (this.stocks.length > 100) { // maximum wat we op 1 scherm laten zien
            this.permitted = 20;
            this.truncated = true;
        }
        this.setView(this.user.stockView);

        // for (const stock of this.stocks) {
        //    this.stocksToUpdate  = this.stocksToUpdate + stock.Company + ',';
        // }
        // this.dataService.getActualquotes(this.stocksToUpdate).subscribe();
        // en nu 8 keer om de 5 seconden de gewijzigde stocks opvragen.
        // TimerObservable.create(0, this.interval)
        // .takeWhile(() => this.retrys > 0)
        //     .subscribe(() => {
        //            console.log('Nieuwe koersen binnenhalen', this.retrys);
        //            this.dataService.loadstocklist(this.user.selectedFilter.Query, this.user.selectedFilter.Stocks, this.user.name)
        //                .subscribe(data => { this.stocks = data; });
        //            this.retrys--;
        //    });
     }

    ngOnDestroy() {
    //    this.retrys = 0; // switches your TimerObservable off
    }

    showAll() {
        this.permitted = 10000;
        this.truncated = false;
        this.setView(this.user.stockView);

    }

    setView(view) {
        if (this.user.isMobile) {
         //   if (view === 'General') {
                this.myColDefs = [this.ColDefsMob[0], this.ColDefsMob[1], this.ColDefs[5], this.ColDefs[7], this.colDefsNaic[0] ];
         //   } else {
         //       this.myColDefs = [this.ColDefsMob[0], this.ColDefs[5], this.colDefsNaic[0], this.colDefsNaic[5], this.colDefsNaic[6]];
         //   }
        } else {
            if (view === 'General') {
                this.myColDefs = this.ColDefs.concat(this.colDefsGenl);
            } else {
                this.myColDefs = this.ColDefs.concat(this.colDefsNaic);
            }
        }
        this.user.stockView = view;
    }

    selectedClass(columnName): string {
        return (columnName === this.user.sort.column) ? this.user.sort.descending : false;
    }

    changeSorting(columnName): void {
        const sort = this.user.sort;
        if (sort.column === columnName) {
            sort.descending = !sort.descending;
        } else {
            sort.column = columnName;
            sort.descending = false;
        }
    }

    convertSorting(): string {
        return this.user.sort.descending ? '-' + this.user.sort.column : this.user.sort.column;
    }

    onUserSelectRow(row, $event) {
        if ($event.target.title.substring(0, 7) === 'Link to') {
            this.fourTraders.open(row);
        } else {
            this.user.stocks = new OrderbyPipe().transform(this.user.stocks, this.convertSorting());
            this.user.setCompany(row.Company);
            if (this.user.isMobile) {
                this.router.navigate(['company', this.user.getCompanyUrl(row.Company), 'naic']);
            } else {
                this.router.navigate(['company', this.user.getCompanyUrl(row.Company), 'general']);
            }
        }
    }

    onShowInfo(columnName) {
        this.infoToShow = columnName;
        this.infoVisible = true;
    }
}
