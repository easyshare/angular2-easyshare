/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { WatchlistResolveService } from './watchlist-resolve.service';

describe('WatchlistResolveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WatchlistResolveService]
    });
  });

  it('should ...', inject([WatchlistResolveService], (service: WatchlistResolveService) => {
    expect(service).toBeTruthy();
  }));
});
