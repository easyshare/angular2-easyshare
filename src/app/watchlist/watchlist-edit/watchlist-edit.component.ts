import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../shared/user.service';
import { DataService } from '../../shared/data.service';
import { Filter } from '../../shared/filter';

@Component({
    selector: 'app-watchlist-edit',
    templateUrl: './watchlist-edit.component.html',
    styleUrls: ['./watchlist-edit.component.css']
})
export class WatchlistEditComponent implements OnInit {
    @Output() filterChanged: EventEmitter<string> = new EventEmitter<string>();

    editFormVisible = false;
    newList: boolean;
    filter = new Filter('', '', '', '', '', false, '', false);
    titel = '';

    constructor(public user: UserService, private data: DataService, private router: Router) { }

    ngOnInit() {
        console.log('watchlist edit component init');
    }

    onSubmit(form) {
        this.editFormVisible = false;
        if (this.newList && (this.filter.Filter === '' )) {
        } else if (this.newList && this.user.findFilter(this.filter.Filter)) {
            alert('Watchlist already exists!');
        } else {
            if (this.newList) {this.filter.Stocks = ','; };
            const curFilter = this.filter.Filter;
            this.data.updateUserFilters(this.user.name, this.filter.Filter, this.filter.Stocks, this.filter.Description, this.filter.Shared)
                .subscribe(data =>
                    this.data.readfilters(this.user.name)
                        .subscribe(filters => {
                            this.user.filters = filters;
                            this.user.setFilter(curFilter);
                            this.filterChanged.emit('Change from nested component');
                        })
                );
        }
        form.reset();
    }

    show(nieuw: boolean) {
        if (nieuw) {
            this.newList = true;
            this.filter = new Filter('', '', '', '', '', false, '', false);
            this.titel = 'Create watchlist ';
        } else {
            this.newList = false;
            this.filter = Object.assign({}, this.user.selectedFilter); // anders wordt huidige filter rechtstreeks gewijzigd
            this.filter.Shared = !!+ this.filter.Shared; // "0" of "1" van mysql naar boolean
            if (this.filter.Stocks === ',') { this.filter.Stocks = ''; }; // nieuw aangemaakte watchlist
            this.titel = 'Edit watchlist '; // + "'" + this.filter.Filter + "'";
        }
        this.editFormVisible = true;
    }

}
