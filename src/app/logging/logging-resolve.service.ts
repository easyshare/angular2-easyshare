import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { DataService } from '../shared/data.service';
import { Observable } from 'rxjs';

@Injectable()
export class LoggingResolveService implements Resolve<any> {

    constructor(private dataService: DataService, private router: Router) { }

    resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | any {
        console.log('logging resolve service - loading log file');
        return this.dataService.readLogging('');
    }
}

