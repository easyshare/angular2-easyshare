import { Component, OnInit } from '@angular/core';
import { DataService } from '../shared/data.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-logging',
    templateUrl: './logging.component.html',
    styleUrls: ['./logging.component.css']
})

export class LoggingComponent implements OnInit {
    data: any;
    dir: any;
    logfile: string;
    logfileName: string;

    constructor(private route: ActivatedRoute, private dataService: DataService) {
        this.route.data.forEach((data: { data: any[] }) => { this.data = data.data; });
    }

    ngOnInit() {
        console.log('logging-component onInit');
        this.formatData();
    }

    showLogfile(file) {
        this.logfileName = file;
        return this.dataService.readLogging(file).subscribe
        (data => { this.data = data; this.formatData(); });
    }

    formatData() {
        this.dir = this.data.dir.split(',');
        this.logfile = this.data.file.split('\n').join('<br/>');
        this.logfileName = this.data.naam;
    }
}
