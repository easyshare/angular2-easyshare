import { TestBed, inject } from '@angular/core/testing';

import { LoggingResolveService } from './logging-resolve.service';

describe('LoggingResolveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoggingResolveService]
    });
  });

  it('should be created', inject([LoggingResolveService], (service: LoggingResolveService) => {
    expect(service).toBeTruthy();
  }));
});
