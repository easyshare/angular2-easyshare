import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { UserService } from '../shared/user.service';
import { DataService } from '../shared/data.service';
import { NewCompanyComponent } from '../new-company/new-company.component';
import { LoginComponent } from '../login/login.component';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { map } from 'rxjs/operators';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
    @Output() loginRequested: EventEmitter<boolean> = new EventEmitter<boolean>();
    @ViewChild(NewCompanyComponent) newCompany: NewCompanyComponent; // wordt niet meer gebruikt - enkel nog in admin component
    @ViewChild(LoginComponent) login: LoginComponent;
    isNavbarCollapsed = true;
    model: any;
    mySource: any;
    sub: any;
    showLogin: boolean;

    constructor(public user: UserService, private data: DataService, private router: Router) {
        console.log('navbar-component init');
    }

    ngOnInit() {}

    loginClick() {
        this.login.loginModalVisible = true;
    }

    // ***********************************************************
    // procedures voor typeahead
    // ***********************************************************
    onFocus() { this.data.searchstocks().subscribe(data => { this.mySource = data; }); }

    formatter = (x: { Name: string }) => x.Name;

    search = (text$: Observable<string>) =>
        text$.pipe(debounceTime(200),
            distinctUntilChanged() )
            .pipe(map(term => term === '' ? []
                : this.mySource.filter(v => new RegExp(term, 'gi').test(v.Tokens)).slice(0, 10)));

    itemSelected($event) {
        $event.preventDefault(); // veeg input leeg;
        this.model = '';
        const Company = $event.item.Company;
        this.user.setCompany(Company);
        this.user.stocks = [];
        this.router.navigate(['/company', this.user.getCompanyUrl(Company), 'dummy']);
    }
    // addCompanyclick() {
    //    this.newCompany.open(this.model);
    // }
}
