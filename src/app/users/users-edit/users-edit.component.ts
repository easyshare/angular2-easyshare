import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from '../../shared/data.service';
import { Router } from '@angular/router';
import { User } from '../user';

@Component({
    selector: 'app-users-edit',
    templateUrl: './users-edit.component.html',
    styleUrls: ['./users-edit.component.css']
})
export class UsersEditComponent implements OnInit {
     @Output() userChanged: EventEmitter<string> = new EventEmitter<string>();

    editFormVisible = false;
    userForm: FormGroup;
    oper = '';
    titel = '';

    constructor(private fb: FormBuilder, private data: DataService, private router: Router) {
        this.createForm();
    }

    ngOnInit() {
    }

    createForm() {
        this.userForm = this.fb.group({
            UserId: ['', Validators.required],
            Name: '',
            Password: ['', Validators.required],
            Email: '',
            Gsm: '',
        });
    }

    show(user, oper) {
        this.userForm.reset(); // alle velden blank
        this.userForm.enable(); // alle velden geenabled
        this.oper = oper;
        this.titel = 'Nieuwe gebruiker';
        if ((oper === 'edit') || (oper === 'del')) {
            this.titel = 'Gebruiker aanpassen';
            this.userForm.patchValue({ UserId: user.UserId });
            this.userForm.patchValue({ Name: user.Name });
            this.userForm.patchValue({ Password: user.Password });
            this.userForm.patchValue({ Email: user.Email });
            this.userForm.patchValue({ Gsm: user.Gsm });
            this.userForm.controls['UserId'].disable();
        }
        if (oper === 'del') {
            this.titel = 'Gebruiker verwijderen?';
            this.userForm.disable();
        }
        this.editFormVisible = true;
    }

    onSubmit() {
        this.userForm.enable(); // alle velden geenabled
        this.data.updateUsers(this.userForm.value, this.oper).subscribe(
            data => {
                 this.userChanged.emit('Change from nested component');
            }
        );
        this.editFormVisible = false;
    }

}
