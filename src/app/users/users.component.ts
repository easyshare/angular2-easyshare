import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../shared/data.service';
import { OrderbyPipe } from '../shared/orderby.pipe';
import { UsersEditComponent } from './users-edit/users-edit.component';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
    @ViewChild(UsersEditComponent) userForm: UsersEditComponent;
    users: any;
    sort: any = {
        column: 'Name',
        descending: false
    };
    myColDefs = [
        { name: 'UserId', index: 'UserId', formatter: 'text' },
        { name: 'Name', index: 'Name', formatter: 'text' },
        { name: 'Email', index: 'Email', formatter: 'text' },
        { name: 'Gsm', index: 'Gsm', formatter: 'text' },
        { name: 'UserType', index: 'UserType', formatter: 'text' },
        { name: 'LastLogin', index: 'LastLogin', formatter: 'text' },
        { name: 'User Agent', index: 'UAgent', formatter: 'text' },
    ];


    constructor(private route: ActivatedRoute, private dataService: DataService) { }

    ngOnInit() {
        console.log('users - component onInit');
        this.route.data.forEach((data: any) => {
            this.users = data.users;
        });
    }

    refresh() {
        this.dataService.readUsers().subscribe(
            data => { this.users = data; });
    }

    convertSorting(): string {
        return this.sort.descending ? '-' + this.sort.column : this.sort.column;
    }

    selectedClass(columnName): string {
        return (columnName === this.sort.column) ? this.sort.descending : false;
    }

    changeSorting(columnName): void {
        const sort = this.sort;
        if (sort.column === columnName) {
            sort.descending = !sort.descending;
        } else {
            sort.column = columnName;
            sort.descending = false;
        }
    }

    onEditUser(user, oper) {
        this.userForm.show(user, oper);
    }

    onUserChanged(message: string) {
        console.log('user data changed');
        this.refresh();
    }

}
