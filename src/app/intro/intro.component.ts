import { Component, OnInit } from '@angular/core';
import { UserService } from '../shared/user.service';
import * as Highcharts from 'highcharts';
require('highcharts/modules/histogram-bellcurve')(Highcharts);
import { DataService} from '../shared/data.service';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.css']
})
export class IntroComponent implements OnInit {
    chart: any;
    Highcharts = Highcharts; // required
    chartCallback = this.saveChart.bind(this);
    chartConstructor = 'chart';
    chartOptions: object;
    scData: Array<object> = [];
    periods = [
        {index: 0, name: ' 5 years', string: 'Perf5Y'},
        {index: 1, name: ' 3 years', string: 'Perf3Y'},
        {index: 2, name: ' 1 year', string: 'Perf1Y'},
    ];
    selectedPeriod = this.periods[2];
    filters = this.user.filters;
    selectedFilter = 'None';
    selectedStocks = '';

    constructor(public user: UserService, private dataService: DataService) {
        this.chartOptions = {
            title: {text: ' '},
            plotOptions: {
                series: { turboThreshold: 5000  }, // set it to a larger threshold, it is by default to 1000
                scatter: { marker: {
                                radius: 5,
                                states: { hover: {enabled: true, lineColor: 'rgb(100,100,100)' } }
                            },
                            states: { hover: { marker: { enabled: false }}},
                            tooltip: {
                                useHTML: true,
                                style: {
                                  pointerEvents: 'auto'
                                },
                                formatter: function() {
                                    return '<b>' + this.name + '</b><br/>Mean yearly perf :' +
                                        this.x + '%<br/>Naic Rating: ' + this.y ;
                                }
                            }
                        }
            },
            xAxis: [ {title: { text: 'Performance' }, alignTicks: true, opposite: false, min: -100, max: 100 } ],
            yAxis: [ {title: { text: 'Naic Rating' }, min: 0, max: 100, opposite: false } ],
            series: [
                {name: 'Company Rating & Performance', yAxis: 0, type: 'scatter', data: [], color: 'rgba(119, 152, 191, .3)',
                    tooltip: {
                        headerFormat: '',
                        pointFormatter: function() {
                            return '<b>' + this.name + '</b><br/>Mean yearly perf :' + this.x + '%<br/>Naic Rating: ' + this.y ; 
                        }
                    },
                    point: {
                        events: {
                            click: function () {
                                location.href = './company/' + this.company;
                            }
                        }
                    }
                },
                {name: 'Index', yAxis: 0, type: 'line', data: [], color: 'rgb(100,100,100)', enableMouseTracking: false },
                // {name: 'relation', yAxis: 0, type: 'line', data: [], color: 'rgb(100,100,100)',enableMouseTracking: false }
            ]
        };
    }

    ngOnInit() {
        console.log('init');
        this.updateChart(this.selectedPeriod.string, this.selectedStocks);
    }

    updateChart(periode: string, stocklist: string) {
        this.dataService.loadperformance(periode, stocklist).subscribe(
            data => {
                 this.chart.series[0].setData([]);
                 this.chart.series[0].setData(data.scdata);
                 this.chart.series[1].setData([[data.meanperf, 100], [data.meanperf, 0]]);
                        // this.chart.series[2].setData(data.regrdata);
                        // this.chColor();
                        // this.chart.redraw();
                     }
        );
    }

    saveChart(chart: any) {
        this.chart = chart;
    }

    chColor() {
        for (let i = 0; i < this.chart.series[0].data.length - 1 ; i++) {
            if (this.chart.series[0].data[i].y > 90) {
                // dit werkt niet
                // this.chart.series[0].data[i].marker = {  fillColor: 'red',  states: { hover: { fillColor: 'red', lineColor: 'red' } } };
                this.chart.series[0].data[i].color = '#CC0000';
                // dit is te traag !
                //this.chart.series[0].data[i].update({
                //    marker: {
                //        fillColor: 'red',
                //        states: {
                //                hover: {
                //                    fillColor: 'red',
                //                    lineColor: 'red'
                //                }
                //        }
                //    }
                // });
            }
        }
    }

    onPeriodClick(period: any) {
        this.selectedPeriod = this.periods[period.index];
        this.updateChart(period.string, this.selectedStocks);
    }

    onFilterClick(filter: any) {
        this.selectedFilter = filter.Filter;
        this.selectedStocks = filter.Stocks;
        this.updateChart(this.selectedPeriod.string,this.selectedStocks);
    }
}
