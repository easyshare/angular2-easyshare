import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Filter } from './filter';

@Injectable()
export class UserService {
    name: string;
    passWord: string;
    userType: string;
    isLoggedIn;
    isMobile;
    selectedFilter: Filter;
    filters: Filter[] = [];
    filterNaam: string;
    stockViews = [];
    stockView: string;
    stocks = [];
    sort: any = { column: 'Macd',  descending: true };
    curCompany: string;
    curStock: any;
    curStocksindex: number;
    curStockList: any;
    curCompanyView: string;
    screenerList: string;

    constructor() {
        console.log('user service initialization');
        this.name = localStorage.getItem('LoggedInUser');
        this.name = this.name ? this.name : 'demo';
        this.passWord = localStorage.getItem('LoggedInUserPw'); // hier doen we verder niets aan
        this.userType = ''; // admin of niet moet komen van de server
        this.isLoggedIn = (false); // moet komen van de server
        this.filterNaam = localStorage.getItem('SelectedWatchList');
        this.selectedFilter = <Filter>{};
        this.stockViews = [{ name: 'General' }, { name: 'Naic' }];
        this.stockView = localStorage.getItem('SelectedView');
        this.stockView = this.stockView ? this.stockView : this.stockViews[0].name;
        this.stocks = [];
        this.curCompany = localStorage.getItem('curCompany');
        this.curCompany = this.curCompany ? this.curCompany : this.setCompany('AAPL');
        this.curCompanyView = 'general';
        this.curStockList = '';
        this.screenerList = localStorage.getItem('screenerList');
        this.screenerList = this.screenerList ? this.screenerList : '';
      //  this.screenerList = this.screenerList ? this.screenerList : 'screpsgst_5yc-scrroe_g10-scrpeg_s15-scrder_s33-screpsgp_g15';
    }

    setUser(naam: string, paswoord: string, usertype: string) {
        this.name = naam;
        this.userType = usertype;
        localStorage.setItem('LoggedInUser', this.name);
        localStorage.setItem('LoggedInUserPw', paswoord);
    }

    setFilter(list: string) {
        let found: number = -1;
        for (let i = 0; i < this.filters.length; i++) {
            if (this.filters[i].Filter === list) { found = i; }
            if (this.filters[i].FilterUrl === undefined) { this.filters[i].FilterUrl = this.filters[i].Filter.replace(/ /g, '_'); }
        }
        if (found < 0) { found = 0; }
        this.selectedFilter = this.filters[found];
        this.selectedFilter.Editable = (this.selectedFilter.UserId.toLowerCase() === this.name.toLocaleLowerCase());
        localStorage.setItem('SelectedWatchList', this.selectedFilter.Filter);
        this.filterNaam = this.selectedFilter.Filter;
    }

    setView(view: string) {
        let found: number = -1;
        for (let i = 0; i < this.stockViews.length; i++) {
            if (this.stockViews[i].name === view) { found = i; }
        }
        if (found < 0) { found = 0; }
        this.stockView = this.stockViews[found].name;
        localStorage.setItem('SelectedView', this.stockView);
    }

    setCompany(comp: string) {
        comp = comp.replace('_', ':'); // bvb XBRU:HOMI
        this.curCompany = comp;
        localStorage.setItem('curCompany', comp);
        this.curStocksindex = -1;
        if (this.stocks.length > 0) {
            this.curStocksindex  = this.stocks.findIndex(x => x.Company === this.curCompany) ;
        }
        return comp;
    }

    setScreenerList(list: string) {
        localStorage.setItem('screenerList', list);
        this.screenerList = localStorage.getItem('screenerList');
        return list;
    }

    setCompanyView(view: string) {
        this.curCompanyView = view;
    }

    getCompanyUrl(comp: string) {
        comp = comp.replace('+', '%2b'); // bvb XBRU:HOMI
        return comp.replace(':', '_');
    }

    getNextCompany() {
        if (this.curStocksindex < this.stocks.length - 1) {
            this.curStocksindex += 1;
        }
        return this.stocks[this.curStocksindex].Company;
    }

    getPrevCompany() {
        if (this.curStocksindex > 0) {
            this.curStocksindex -= 1;
        }
        return this.stocks[this.curStocksindex].Company;
    }

    findFilter(list: string) {
        let found = false;
        for (let i = 0; i < this.filters.length; i++) {
            if (this.filters[i].Filter === list) { found = true; }
        }
        return found;
    }
}
