export class Filter {
    constructor(
    public UserId: string,
    public Description: string,
    public Filter: string,
    public FilterUrl: string,
    public Query: string,
    public Shared: boolean,
    public Stocks: string,
    public Editable: boolean,
    ) {}
}
