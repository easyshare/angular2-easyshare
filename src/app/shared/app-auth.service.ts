import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { DataService } from './data.service';
import { UserService } from './user.service';

@Injectable()
export class AppAuthService implements CanActivate {

    constructor(private user: UserService, private dataService: DataService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (!this.user.isLoggedIn) {
            console.log('in de canactivate maar nog niet ingelogd');
            this.dataService.login(this.user.name, this.user.passWord)
                .subscribe(data => {
                    data.Loggedin ? this.user.userType = data.UserType : this.user.setUser('demo', '', '');
                    this.dataService.readfilters(this.user.name)
                        .subscribe(filters => {
                            this.user.filters = filters;
                            this.user.setFilter(this.user.filterNaam);
                            this.user.isLoggedIn = (true);
                            this.router.navigate([state.url]);
                        });
                });
            return false;
        } else {
            if (state.url === '/users' && this.user.userType !== 'Admin') {
                return false;
            } else {
                if (state.url === '/logging' && this.user.userType !== 'Admin') {
                    return false;
                } else {
                    if (state.url === '/admin' && this.user.userType !== 'Admin') {
                        return false;
                    } else {
                        console.log('Ingelogd - Nieuwe route :', state.url);
                    return true;
                    }
                }
            }
        }
    }
}
