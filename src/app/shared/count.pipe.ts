import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'count',
    pure: false
})
export class CountPipe implements PipeTransform {
    transform(items: any[], count: number): any {
        if (!items || !count) {
            return items;
        }
        count = items.length > count ? count : items.length;
        return items.slice(0, count);
    }
}