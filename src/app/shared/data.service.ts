import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';


@Injectable()
export class DataService {

    baseUrl = 'https://easyshare.basex.be/php/';
    // baseUrl = 'http://localhost/eshare/php/';
    constructor(private _http: HttpClient) {
    }

    login(name: string, password: string): Observable<any> {
        const _Url = this.baseUrl + 'clientserver/login.php?user_name=' + name + '&password=' + password;
        return this._http.get(_Url).pipe(map(data => data), catchError(this.handleError));
    }

    readfilters(name: string): Observable<any> {
        const _Url = this.baseUrl + 'clientserver/readfilters.php?user_name=' + name;
        return this._http.get(_Url).pipe(map(data => data), catchError(this.handleError));
    }

    loadstocklist(query: string, stocklist: string, userid: string): Observable<any> {
        const _Url = this.baseUrl + 'clientserver/Updatestocks.php?oper=read&query=' +
            query + '&stocklist=' + stocklist + '&user_name=' + userid + '&time=' + new Date().getTime();
        return this._http.get(_Url).pipe(map(data => data), catchError(this.handleError));
    }
    loadnaic(company: string, userid: string): Observable<any>  {
        const _Url = this.baseUrl + 'clientserver/updateNaic.php?oper=read&Company=' + company + '&user_name=' + userid;
        return this._http.get(_Url).pipe(map(data => data), catchError(this.handleError));
    }
    loadperformance(period: string, stocklist: string): Observable<any> {
        const _Url = this.baseUrl + 'clientserver/NaicPerformanceRelation.php?periode=' + period + '&stocklist=' + stocklist;
        return this._http.get(_Url).pipe(map(data => data), catchError(this.handleError));
    }
    searchstocks() {
        const _Url = this.baseUrl + 'clientserver/searchStocks.php';
        return this._http.get(_Url).pipe(map(data => data), catchError(this.handleError));
    }
    readYahooNews(yahooSymbol: string) {
        const _Url = this.baseUrl + 'clientserver/readyahoonews.php?symbol=' + yahooSymbol;
        return this._http.get(_Url).pipe(map(data => data), catchError(this.handleError));
    }
    readYahooQuotes(yahooSymbol: string) {
        const _Url = this.baseUrl + 'clientserver/readquotes.php?symbol=' + yahooSymbol;
        return this._http.get(_Url).pipe(map(data => data), catchError(this.handleError));
    }
    addCompany(msnticker: string, yahooticker: string) {
        const _Url = this.baseUrl + 'clientserver/companydata.php?symbol=' + msnticker + '&yahooticker=' + yahooticker + '&oper=add';
        return this._http.get(_Url).pipe(map(data => data), catchError(this.handleError));
    }
    readUsers() {
        const _Url = this.baseUrl + 'clientserver/updateUsers.php';
        return this._http.get(_Url).pipe(map(data => data), catchError(this.handleError));
    }
    updateUsers(user, oper) {
        const _Url = this.baseUrl + 'clientserver/updateUsers.php';
        let body: HttpParams = new HttpParams();
        Object.keys(user).forEach(function (value) {body = body.append(value, user[value]);});
        body = body.append('oper', oper);
        return this._http.post(_Url, body).pipe(catchError(this.handleError));
    }

    updateNaic( naicstudy) {
        const _Url = this.baseUrl + 'clientserver/updateNaic.php';
        let body: HttpParams  = new HttpParams();
        Object.keys(naicstudy).forEach(function (value) { body = body.append(value, naicstudy[value]); });
        return this._http.get(_Url, {params: body}).pipe(catchError(this.handleError));
    }
    update4TTicker(company: string, ticker: string) {
        const _Url = this.baseUrl + './clientserver/update4Tticker.php?company=' + company + '&fourtrt=' + ticker;
        return this._http.get(_Url).pipe(map(data => data), catchError(this.handleError));
    }
    updateUserFilters(userid: string, lijstnaam: string, stocklist: string, desc: string, shared: boolean) {
        const _Url = this.baseUrl + './clientserver/UpdateUserFilters.php?user_name=' + userid + '&filter_name=' + lijstnaam
            + '&stocks_lijst=' + stocklist + '&desc=' + desc + '&shared=' + shared.valueOf();
        return this._http.get(_Url).pipe(map(data => data), catchError(this.handleError));
    }
    updateStocks(oper: string, company: string, yahooticker: string) {
        const _Url = this.baseUrl + 'clientserver/Updatestocks.php?symbol=' + encodeURIComponent(yahooticker)
            + '&msn=' + encodeURIComponent(company) + '&oper=' + oper;
        return this._http.get(_Url).pipe(map(data => data), catchError(this.handleError));
    }
    getActualquotes(stocks: string) {
        const _Url = this.baseUrl + 'utilities/actualizequotes.php?stocks=' + stocks;
        return this._http.get(_Url).pipe(map(data => data), catchError(this.handleError));
    }
    readLogging(logfile: string) {
        const _Url = this.baseUrl + 'clientserver/readLogging.php?file=' + encodeURIComponent(logfile);
        return this._http.get(_Url).pipe(map(data => data), catchError(this.handleError));
    }

    private handleError(error: any) {
        const errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return throwError(errMsg);
    }
}
