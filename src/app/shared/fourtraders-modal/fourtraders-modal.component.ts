import { Component, OnInit } from '@angular/core';
import { DataService } from '../../shared/data.service';

@Component({
    selector: 'app-fourtraders-modal',
    templateUrl: './fourtraders-modal.component.html',
    styleUrls: ['./fourtraders-modal.component.css']
})
export class FourTradersModalComponent implements OnInit {
    modalVisible = false;
    no4TradersSymbol = true;
    fourTradersLink = '';
    companyName = '';
    row: any;
    server = 'www.marketscreener.com';

    constructor(private dataService: DataService) { }

    ngOnInit() {
        console.log('4Taders modal component onInit');
    }

    open(row) {
        this.row = row;
        this.fourTradersLink = '';
        this.no4TradersSymbol = true;
        this.companyName = this.row.Name;
        if (this.row['4TradersSymbol'] !== '') {
            this.fourTradersLink = this.row['4TradersSymbol'];
            this.no4TradersSymbol = false;
        }
        this.modalVisible = true;
    }

    onGetItClick() {
        this.fourTradersLink = this.fourTradersLink.toUpperCase();
        if (this.fourTradersLink !== this.row['4TradersSymbol'].toUpperCase()) {
            // hola er moet iets gebeuren. Zit er 4-traders in de string?
            let pos = this.fourTradersLink.indexOf(this.server.toUpperCase());
            if (pos > -1) { this.fourTradersLink = this.fourTradersLink.substring(pos + this.server.length + 1); }
            // zit er nog een / in de input?
            pos = this.fourTradersLink.indexOf('/');
            if (pos > -1) { this.fourTradersLink = this.fourTradersLink.substring(0, pos); }
            // dit moeten we nu nog wegzetten
            this.row['4TradersSymbol'] = this.fourTradersLink;
            this.dataService.update4TTicker(this.row.Company, this.fourTradersLink).subscribe();
        }
        if (this.fourTradersLink.length === 0) {
            window.open('http://' + this.server);
        } else {
            window.open('http://' + this.server + '/' + this.fourTradersLink + '/');
        }
        this.modalVisible = false;
    }

}
