import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FourTradersModalComponent } from './fourtraders-modal.component';

describe('FourtTradersModalComponent', () => {
  let component: FourTradersModalComponent;
  let fixture: ComponentFixture<FourTradersModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FourTradersModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FourTradersModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
