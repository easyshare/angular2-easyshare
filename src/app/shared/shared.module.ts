import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormatPipe } from './format.pipe';
import { OrderbyPipe } from './orderby.pipe';
import { CountPipe } from './count.pipe';
import { UserService } from './user.service';
import { DataService } from './data.service';
import { FunctionsService } from './functions.service';
import { PageNotFoundComponent} from './page-not-found.component';
import { FormsModule } from '@angular/forms';
import { FourTradersModalComponent } from './fourtraders-modal/fourtraders-modal.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    FormatPipe,
    CountPipe,
    OrderbyPipe,
    PageNotFoundComponent,
    FourTradersModalComponent
  ],
  exports: [
    FormatPipe,
    CountPipe,
    OrderbyPipe,
    FourTradersModalComponent
  ],
   providers: [UserService, DataService, FunctionsService ],
})
export class SharedModule { }
