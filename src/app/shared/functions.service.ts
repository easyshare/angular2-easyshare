import { Injectable } from '@angular/core';

@Injectable()
export class FunctionsService {

    constructor() { }

    calculateStock(stock) {
        const kz = this.koersZones(+stock.Value, +stock.LaagstePrijs, +stock.HoogstePrijs, 25, 50);
        stock.Advice = kz.Advice;
        const expGemWpa = this.expGemWpa(+stock.LaagsteWpa, +stock.WpaGroei, 5);
        stock.DivReturn = this.expDividend(expGemWpa, stock.ExpPayout, stock.Value);
        const expRateReturn = (stock.HoogstePrijs > 0) ? this.cagr(stock.Value, +stock.HoogstePrijs, 5) : null;
        stock.TotReturn = (stock.HoogstePrijs > 0) ? this.round(expRateReturn + stock.DivReturn, 1) : null;
    }

    calculateNaic(naicdata) {
        if (naicdata.enoughdata) {
            if (naicdata.Company.substring(0, 4) === 'XLON') {
            //    naicdata.PeHist.AvgHpe = naicdata.PeHist.AvgHpe / 100;
            //    naicdata.PeHist.AvgLpe = naicdata.PeHist.AvgLpe / 100;
            //    const series = naicdata.PeHist.Series;
            //    for (let i = 0; i < series.length; i++) {
            //        series[i].Eps = series[i].Eps * 100;
            //    }
            //    for (let teller = naicdata.epsseries.length - 1; teller >= 0; teller -= 1) {
            //        naicdata.epsseries[teller][1] = naicdata.epsseries[teller][1] * 100;
            //    }
            //    if (naicdata.epsest1serie.length > 0) {
            //        naicdata.epsest1serie[0][1] = naicdata.epsest1serie[0][1] * 100;
            //    }
            //    if (naicdata.epsest2serie.length > 0) {
            //        naicdata.epsest2serie[0][1] = naicdata.epsest2serie[0][1] * 100;
            //    }
            }
            if (!naicdata.NaicStudy.hasOwnProperty('WpaGroei')) {
                // geen bestaande naicStudy - We nemen default values om te berekenen
                naicdata.savedStudy = false;
                naicdata.NaicStudy.LaagsteWpa = naicdata.NewResults[naicdata.LastBookYear].Eps;
                naicdata.NaicStudy.OmzetGroei = naicdata.histGroei.Omzet;
                naicdata.NaicStudy.WpaGroei = naicdata.histGroei.Wpa;
                naicdata.NaicStudy.HoogsteWpa =
                    this.futureValue(naicdata.NaicStudy.LaagsteWpa, naicdata.NaicStudy.WpaGroei, 5);
                naicdata.NaicStudy.HoogsteKW = naicdata.PeHist.AvgHpe;
                naicdata.NaicStudy.LaagsteKW = naicdata.PeHist.AvgLpe;
                naicdata.NaicStudy.ExpPayout = naicdata.PeHist.Payout;
                naicdata.NaicStudy.LaagstePrijs = Math.round(naicdata.NaicStudy.LaagsteKW * naicdata.NaicStudy.LaagsteWpa * 10) / 10;
                naicdata.NaicStudy.HoogstePrijs = Math.round(naicdata.NaicStudy.HoogsteKW * naicdata.NaicStudy.HoogsteWpa * 10) / 10;
            } else {
                naicdata.savedStudy = true;
            }
        }
    }

    calculateStars(stock) {
        let stars = null;
        if ((stock.OmzetGroei !== null) && (stock.WpaGroei !== null)) {
            stars = 0;
            if (this.calcStarOmzet(stock.OmzetGroei)) { stars += 1; }
            if (this.calcStarEps(stock.WpaGroei)) { stars += 1; }
            if (this.calcStarProfit(stock.NetProfitMargin)) { stars += 1; }
            if (this.calcStarRoe(stock.ReturnOnEquity)) { stars += 1; }
            if (this.calcStarDebt(stock.DebtEquityRatio )) { stars += 1; }
        }
        return stars;
    }

    calcStarOmzet(omzetgroei: number) { return omzetgroei >= 10 ? true : false; }

    calcStarEps(epsgroei: number) { return epsgroei >= 10 ? true : false; }

    calcStarProfit(profit: number) { return profit >= 10 ? true : false; }

    calcStarRoe(roe: number) { return roe >= 10 ? true : false; }

    calcStarDebt(debt: number) { return debt < 0.33 ? true : false; }


    expGemWpa(wpa, groei, jaren) {
        // eerst berekenen we de gemiddelde wpa voor de volgende jaren
        const factor = (groei === 0) ? 1 : 1 + groei / 100;
        let totWpa = 0;
        for (let teller = 0; teller < jaren; teller += 1) {
            wpa = wpa * factor;
            totWpa += wpa;
        }
        return this.round(totWpa / jaren, 1);
    }

    expDividend(expGemWpa, expPayout, curValue) {
        let result = null;
        if (expPayout > 0) {
            result = this.round(expGemWpa * expPayout / curValue, 1);
        }
        return result;
    }

    dateUtc(datum: string) {
        return Date.UTC(parseInt(datum.substring(0, 4), 10),
            parseInt(datum.substring(5, 7), 10) - 1, parseInt(datum.substring(8, 10), 10));
    }

    cagr(y1, y2, jaren) {
        // Compound annual Growth rate
        const f1 = y2 / y1;
        const f2 = 1 / jaren;
        const groei = Math.pow(f1, f2);
        return this.round((groei - 1) * 100, 1);
    }

    round(rnum, rlength) {// Arguments: number to round, number of decimal places
        return Math.round(rnum * Math.pow(10, rlength)) / +Math.pow(10, rlength).toFixed(1);
    }

    koersZones(current, laagstePrijs, hoogstePrijs, buyPercent, holdPercent) {
        let buyLimit = 0, holdLimit = 0, advice = '', verschil;
        if (hoogstePrijs > laagstePrijs) {
            verschil = hoogstePrijs - laagstePrijs;
            buyLimit = this.round(laagstePrijs + verschil * buyPercent / 100, 1);
            holdLimit = this.round(laagstePrijs + verschil * (buyPercent + holdPercent) / 100, 1);
            advice = 'BUY';
            if (current <= buyLimit) {
                advice = 'BUY';
            } else {
                if (current <= holdLimit) {
                    advice = 'MAYBE';
                } else {
                    advice = 'SELL';
                }
            }
        }
        return {
            Advice: advice,
            BuyLimit: buyLimit,
            HoldLimit: holdLimit,
            HlRatio: (hoogstePrijs - current) / (current - laagstePrijs),
            Appreciation: hoogstePrijs / current * 100 - 100
        };
    }

    futureValue(y1, groei, jaren) {
        const factor = (groei === 0) ? 1 : 1 + groei / 100;
        return this.round(y1 * Math.pow(factor, jaren), 2);
    }

}

