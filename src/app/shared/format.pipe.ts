import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe, DecimalPipe } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
    name: 'format'
})
export class FormatPipe implements PipeTransform {

    // transform(value: any, args?: any): any {
    //   return null;
    // }

    datePipe: DatePipe;
    decimalPipe: DecimalPipe;

    constructor(public _sanitizer: DomSanitizer) { }

    transform(input: string, args: any, args1: any): any {
        let cellHtml = '';
        let format = '';
        let parsedFloat = 0;
        const pipeArgs = args.split(':');
        for (let i = 0; i < pipeArgs.length; i++) {
            pipeArgs[i] = pipeArgs[i].trim(' ');
        }

        switch (pipeArgs[0].toLowerCase()) {
            case 'text':
                return input;
            case 'integer':
                return parseInt(input, 10);
            case 'decimal':
            case 'number':
                parsedFloat = !isNaN(parseFloat(input)) ? parseFloat(input) : 0;
                format = pipeArgs.length > 1 ? pipeArgs[1] : null;
                return this.decimalPipe.transform(parsedFloat, format);
            case 'percentage':
                parsedFloat = !isNaN(parseFloat(input)) ? parseFloat(input) : 0;
                format = pipeArgs.length > 1 ? pipeArgs[1] : null;
                return this.decimalPipe.transform(parsedFloat, format) + '%';
            case 'myboldperc':
                if (input === null || input === '') { return ''; }
                const color = +input > 0 ? 'green' : 'red';
                return this._sanitizer.bypassSecurityTrustHtml('<span style="color:' + color + ';">' + input + '%</span>');
            case 'companyname':
                cellHtml = '<strong>' + args1['Company'] + '</strong></br><span style="font-size:9px;">' + args1['Name'] + '</span>';
                return this._sanitizer.bypassSecurityTrustHtml(cellHtml);
            case 'datevalue':
                cellHtml = '<strong>' + args1['Value'] + '</strong></br><span style="font-size:9px;">' + args1['RateDate'] + '</span>';
                return this._sanitizer.bypassSecurityTrustHtml(cellHtml);
            case 'mypercent':
                parsedFloat = !isNaN(parseFloat(input)) ? parseFloat(input) : 0;
                return (Math.round(parsedFloat * 10) / 10).toFixed(1) + '%';
            case 'mytrend':
                if (input === null || input === '') {
                    return '';
                }
                cellHtml = '<span [ngStyle] ="{letter-spacing:0cm;}">';
                for (let i = 0; i < input.length; i += 1) {
                    const ch = input.charAt(i);
                    switch (ch) {
                        case '0':
                            cellHtml += '<span style = "color: red" >' + '&#9660;' + '</span>';
                            break;
                        case '1':
                            cellHtml += '<span style = "color:#ffc0cb">' + '&#9660;' + '</span>';
                            break;
                        case '2':
                            cellHtml += '<span style = "color:lime">' + '&#9650;' + '</span>';
                            break;
                        case '3':
                            cellHtml += '<span style = "color:green">' + '&#9650;' + '</span>';
                            break;
                        default:
                            cellHtml += '';
                    }
                }
                return this._sanitizer.bypassSecurityTrustHtml(cellHtml + '</span>');
            case 'mydate':
                let result = '';
                const first_part = input.split(' ')[0];
                const year = first_part.split('-')[0];
                const month = first_part.split('-')[1];
                const day = first_part.split('-')[2];
                const last_part = input.split(' ')[1];
                const hour = last_part.split(':')[0];
                const minute = last_part.split(':')[1];
                const second = last_part.split(':')[2];
                const hulp = new Date(+year, +month - 1, +day, +hour, +minute, +second); // yahoo date object with local timezone
                const localOffset = hulp.getTimezoneOffset() * 60000;
                // yahootijden zijn altijd ET timezone  of een timezoneoffset van 4 * 60 minuten met GMT
                // 01/11/2012 ET heeft nu ook daylightsaving en tot 2de zondag van maart 5 * 60 minuten met GMT
                const gmt = hulp.getTime() + 5 * 60 * 60 * 1000;
                let datum = new Date(gmt - localOffset);
                datum = hulp; // uitschakelen conversie
                const nu = new Date();
                if (nu.getDate() === datum.getDate() && nu.getMonth() === datum.getMonth() && nu.getFullYear() === datum.getFullYear()) {
                    let min = datum.getMinutes().toString();
                    if (min.length === 1) { min = '0' + min; }
                    result = datum.getHours() + ':' + min;
                } else {
                    const jaar = datum.getFullYear();
                    result = datum.getDate() + '/' + (+datum.getMonth() + 1) + '/' + jaar.toString().substr(2, 2);
                }
                //  cellHtml = '<span>' + result + '</span>';
                return result;
            case 'datetime':
                const date = !isNaN(parseInt(input, 10)) ? parseInt(input, 10) : new Date(+input, 10);
                format = 'MMM d, y h:mm:ss a';
                if (pipeArgs.length > 1) {
                    format = '';
                    for (let i = 1; i < pipeArgs.length; i++) {
                        format += pipeArgs[i];
                    }
                }
                return this.datePipe.transform(date, format);
            case 'mystars':
                cellHtml = '';
                for (let teller = 0; teller < +input; teller += 1) {
                    cellHtml += '<span style="color:green"><i class="fa fa-star"></i></span>';
                }
                for (let teller = 0; teller < 5 - (+input); teller += 1) {
                    cellHtml += '<span style="color:lightgrey"><i class="fa fa-star"></i></span>';
                }
                return this._sanitizer.bypassSecurityTrustHtml(cellHtml);
            case 'my4traders':
                const title = args1['4TradersSymbol'].length === 0 ? 'Link to 4-Traders' : 'Link to ' + args1['Company'];
                const formatted_cellval = "<img src = 'assets/img/4-traders.ico' title='" + title + "' style='height:16px' >";
                if (args1['4TradersSymbol'].length === 0) {
                    cellHtml = '<span style="background-color:yellow;">&nbsp;' + formatted_cellval + '&nbsp;</span>';
                } else {
                    cellHtml = '&nbsp;' + formatted_cellval + '&nbsp;';
                }
                return this._sanitizer.bypassSecurityTrustHtml(cellHtml);
            default:
                return input;
        }
    }
}
