import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HowtoComponent } from './howto/howto.component';
import { AdminComponent } from './admin/admin.component';
import { IntroComponent } from './intro/intro.component';
import { LoggingComponent } from './logging/logging.component';
import { LoggingResolveService } from './logging/logging-resolve.service';
import { AppAuthService } from './shared/app-auth.service';
import { PageNotFoundComponent} from './shared/page-not-found.component';

const routes: Routes = [
    { path: '', redirectTo: '/intro', pathMatch: 'full' },
    { path: 'howto', canActivate: [AppAuthService], component: HowtoComponent },
    { path: 'admin', canActivate: [AppAuthService], component: AdminComponent },
    { path: 'intro', canActivate: [AppAuthService], component: IntroComponent },
    { path: 'logging', canActivate: [AppAuthService], component: LoggingComponent , resolve: { data: LoggingResolveService }},
    { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    // imports: [RouterModule.forRoot(routes, { enableTracing: true })],
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: []
})
export class AppRoutingModule { }
