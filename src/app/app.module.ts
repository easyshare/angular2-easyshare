import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { CompanyModule } from './company/company.module';
import { WatchlistModule } from './watchlist/watchlist.module';
import { NavbarComponent } from './navbar/navbar.component';
import { HowtoComponent } from './howto/howto.component';

import { SharedModule } from './shared/shared.module';
import { HighchartsChartModule } from 'highcharts-angular';
import { AppAuthService } from './shared/app-auth.service';

import { LoginComponent } from './login/login.component';
import { DummyComponent } from './company/dummy/dummy.component';
import { NewCompanyComponent } from './new-company/new-company.component';
import { UsersComponent } from './users/users.component';
import { UsersResolveService } from './users/users-resolve.service';
import { UsersEditComponent } from './users/users-edit/users-edit.component';
import { LoggingComponent } from './logging/logging.component';
import { LoggingResolveService } from './logging/logging-resolve.service';
import { AdminComponent } from './admin/admin.component';
import { IntroComponent } from './intro/intro.component';

@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent,
        HowtoComponent,
        LoginComponent,
        DummyComponent,
        NewCompanyComponent,
        UsersComponent,
        UsersEditComponent,
        LoggingComponent,
        AdminComponent,
        IntroComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        NgbModule.forRoot(),
        HighchartsChartModule,
        CompanyModule,
        WatchlistModule,
        AppRoutingModule, // als laatste omwile van ** path !
        SharedModule,
        ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
    ],

    providers: [AppAuthService, UsersResolveService, LoggingResolveService ],
    bootstrap: [AppComponent]
})
export class AppModule { }
