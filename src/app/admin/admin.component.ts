import { Component, OnInit, ViewChild  } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { throwError } from 'rxjs';
import { NewCompanyComponent } from '../new-company/new-company.component';
import { map, catchError } from 'rxjs/operators';
import { DataService} from '../shared/data.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  @ViewChild(NewCompanyComponent) newCompany: NewCompanyComponent;
  output = '<br>';
  curIndex = 0;
  tickers = new Array();
  baseUrl = 'https://easyshare.basex.be/flask/';
  // baseUrl = 'http://localhost:5000/';

  constructor(private _http: HttpClient, private dataService: DataService) {}

  ngOnInit() {}

  onRefreshCompanyClick() {
    this.output = '<br>';
    this.newCompany.open(true, true);
  }

  onNewCompanyClick() {
    this.output = '<br>';
    this.newCompany.open(true, false);
  }

  onAutomaticClick() {
    const days = 20;
    let lastHistDate: Date;
    const now: Date = new Date();
    this.output = '<br>';
    this.dataService.loadstocklist('1', '', '')
    .subscribe(data => {
        Object.keys(data).forEach(key => {
            const stock = data[key];
            lastHistDate = new Date(stock.LastUpdate);
            const diff = Math.abs(now.getTime() - lastHistDate.getTime());
            if (Math.ceil(diff / (1000 * 3600 * 24)) > days ) {
                this.tickers.push(stock.Company);
            }
        });
        this.output += '<h4>Starting for updating ' + this.tickers.length.toString() ;
        this.output += ' stocks whithout update for the last ' + days.toString() + ' days!</h4><br>';
        this.updateNextTicker();
    });
  }

  onNaicClick() {
    this.output = '<br>';
    this.dataService.loadstocklist('1', '', '')
    .subscribe(data => {
        for (const stock of data) {
            this.dataService.loadnaic(stock.Company, 'ivo')
            .subscribe(data1 => {
               if (data1.Analysis.AlgTotaal >= 95) {
                    this.output += stock.Company + ' heeft ' + data1.Analysis.AlgTotaal + ' punten.<br>';
               }
            });
        }
    } );
  }

  onInputCompanyReady($event: any) {
      this.output += ($event.refresh ? 'updating ' : 'new symbol ') + $event.tickerM + ' Please wait .....<br>';
      this.refreshCompany($event.refresh, $event.tickerM, $event.tickerY).subscribe(data => this.output += data );
  }

  refreshCompany(refresh: boolean, symbol: string, yahoo: string) {
    const _Url = this.baseUrl + 'updatecompany?symbol=' + symbol + '&refresh=' + refresh + '&yahoo=' + yahoo;
   // return this._http.get(_Url).pipe(map(data => data.json()), catchError(this.handleError));
   return this._http.get(_Url);
  }

  async delay(ms: number) {
    await new Promise(resolve => setTimeout(() => resolve(), ms)).then(() => console.log('fired'));
  }

  updateNextTicker() {
    if (this.curIndex < this.tickers.length) {
        const ticker = this.tickers[this.curIndex];
        this.output += 'waiting for updating ' + ticker + ' ......<br>';
        // minstens 60 seconden wachten dan is de vorige update zeker klaar
        this.delay(120000).then(any => {
            this.output += 'updating ' + ticker + '<br>';
            this.refreshCompany(true, ticker, '' ).subscribe(data => this.output += data );
            this.curIndex = this.curIndex + 1;
            this.updateNextTicker();
        });
    }
  }

  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return throwError(errMsg);
}

}
