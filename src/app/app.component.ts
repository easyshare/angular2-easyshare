import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { UserService } from './shared/user.service';
import { Title } from '@angular/platform-browser';
import { filter } from 'rxjs/operators';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private user: UserService,
        private titleService: Title
    ) { }

    ngOnInit() {
        // ***************************************************
        // **************  hier worden de paginatitels gezet
        // ***************************************************
        this.router.events.pipe(
            filter((event) => event instanceof NavigationEnd) )
            .subscribe((event) => {
                const pathArray = window.location.pathname.split('/');
                let titel = '';
                if (pathArray[1] === 'watchlist') {
                    titel = pathArray[2];
                } else {
                    if (pathArray[1] === 'company') {
                        titel = 'Company ' + pathArray[2];
                    } else {
                        titel = pathArray[1];
                        titel = titel.charAt(0).toUpperCase() + titel.slice(1);
                    }
                }
                this.titleService.setTitle(titel);
            });
        // *******************************************************
        // *********** zitten we op mobile of niet
        // *******************************************************
        this.user.isMobile = false;
        console.log(window.innerWidth);
        if (window.innerWidth <= 600) { // && window.innerHeight <= 600)
            this.user.isMobile = true;
        }
    }
}
