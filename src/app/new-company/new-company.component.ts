import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../shared/user.service';
import { DataService } from '../shared/data.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-new-company',
    templateUrl: './new-company.component.html',
    styleUrls: ['./new-company.component.css']
})

export class NewCompanyComponent implements OnInit {
    @Output() inputCompanyReady = new EventEmitter();
    modalAdminVisible = false;
    modalUserVisible = false;
    buttonText = '';
    titel = '';
    companyForm = this.fb.group({
        TickerM: ['', Validators.required],
        TickerY: ['', Validators.required],
    });


    constructor(private modalService: NgbModal, private fb: FormBuilder, private data: DataService,
        private router: Router, private user: UserService) { }

    ngOnInit() {  console.log('new company init'); }

    onAdminSubmit() {
        const tickerM = this.companyForm.get('TickerM').value;
        const tickerY = this.companyForm.get('TickerY').value;
        const refresh = this.companyForm.controls.TickerY.disabled;
        this.inputCompanyReady.emit({refresh: refresh, tickerM: tickerM , tickerY: tickerY } );
        this.modalAdminVisible = false;
    }

    onUserSubmit() {
        window.open('mailto:ivo.elen@gmail.com?&subject=Aanvraag nieuw aandeel Easyshare&body=');
        this.modalUserVisible = false;
    }

    onChangeSymbol(){
        let morningstarSymbol = this.companyForm.get('TickerM').value;
        if (morningstarSymbol) {
            morningstarSymbol = morningstarSymbol.toUpperCase();
            // this.companyForm.disable();
            let yahooSymbol = '';
            const parts = morningstarSymbol.split(':');
            if (parts.length > 1) {
                //  25/01/2019  + niet meer gebruikt door Morningstar
                // parts[1] = parts[1].replace('+', '-');
                parts[1] = parts[1].replace('.', '-');
                parts[1] = parts[1].replace(' ', '-');
                switch (parts[0]) {
                    case 'XAMS': yahooSymbol = parts[1] + '.AS'; break;
                    case 'XBRU': yahooSymbol = parts[1] + '.BR'; break;
                    case 'XETR': yahooSymbol = parts[1] + '.DE'; break;
                    case 'XFRA': yahooSymbol = parts[1] + '.F'; break;
                    case 'XSHG': yahooSymbol = parts[1] + '.SS'; break;
                    case 'XSWX': yahooSymbol = parts[1] + '.VX'; break;
                    // case 'XSWX': yahooSymbol= parts[1]+'.SW';break;
                    case 'XCSE': yahooSymbol = parts[1] + '.CO'; break;
                    case 'XMCE': yahooSymbol = parts[1] + '.MC'; break;
                    case 'XPAR': yahooSymbol = parts[1] + '.PA'; break;
                    case 'XLON':
                        yahooSymbol = parts[1] + '.L';
                        if (yahooSymbol === 'RR-.L') { yahooSymbol = 'RR.L'; }
                        break;
                    case 'XMIL': yahooSymbol = parts[1] + '.MI'; break;
                    case 'XSTO': yahooSymbol = parts[1] + '.ST'; break;
                    case 'XTSE': yahooSymbol = parts[1] + '.TO'; break;
                    case 'XASX': yahooSymbol = parts[1] + '.AX'; break;
                    case 'XSHE': yahooSymbol = parts[1] + '.HK'; break;
                    case 'XHKG': yahooSymbol = parts[1] + '.HK'; break;
                }
            } else {
                yahooSymbol = morningstarSymbol;
                yahooSymbol = yahooSymbol.replace('+', '-');
                yahooSymbol = yahooSymbol.replace('.', '-');
                yahooSymbol = yahooSymbol.replace(' ', '-');
            }
            this.companyForm.patchValue({ TickerM: morningstarSymbol });
            this.companyForm.patchValue({ TickerY: yahooSymbol });
        }
    }

    open(admin: boolean, refresh: boolean = null) {
        this.companyForm.markAsPristine()
        this.companyForm.patchValue({ TickerM: '' });
        this.companyForm.patchValue({ TickerY: '' });
        this.buttonText = 'Add!';
        this.titel = 'New Easyshare Company';
        if (admin) {
            this.modalAdminVisible = true;
            if (refresh) {
                this.buttonText = 'Update!';
                this.titel = 'Update Easyshare Company';
                this.companyForm.controls.TickerY.disable();
            } else {
                this.companyForm.controls.TickerY.enable();
            }
        } else {
            this.modalUserVisible = true;
        }
    }
}
