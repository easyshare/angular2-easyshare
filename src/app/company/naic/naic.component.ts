import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../shared/user.service';
import { FunctionsService } from '../../shared/functions.service';
import { ActivatedRoute } from '@angular/router';
import { NaicChartComponent } from './naic-chart/naic-chart.component';


@Component({
  selector: 'app-naic',
  templateUrl: './naic.component.html',
  styleUrls: ['./naic.component.css']
})

export class NaicComponent implements OnInit {

  @ViewChild(NaicChartComponent)
  naicChart: NaicChartComponent;
  naicdata: any;
  chartHeight: any;

  constructor(private user: UserService, private route: ActivatedRoute, private f: FunctionsService) {
    this.route.data.forEach((data: { data: any[] }) => {
        this.naicdata = data.data;
        this.f.calculateNaic(this.naicdata);
    });
  }

  ngOnInit() {
    console.log('Naic-component onInit');
    this.user.setCompanyView('naic');
    this.chartHeight = window.screen.width < 576 ? window.screen.height - 300 : window.screen.height - 300;
  }

  ChartResetRequest($event) { this.naicChart.ResetZoom(); }

  ChartZoomKWRequest($event) { this.naicChart.ZoomKW(); }

  ChartZoomPriceRequest($event) { }

  ChartUpdateRequest($event) { this.naicChart.ChartUpdate(); }

}


