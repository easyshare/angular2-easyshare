import { Component, OnInit, Input, Output, ViewChild, ElementRef, EventEmitter, AfterViewInit } from '@angular/core';
import { NgbPanelChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { NgbAccordionConfig} from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../../../shared/user.service';
import { DataService } from '../../../shared/data.service';
import { FunctionsService } from '../../../shared/functions.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-naic-accordeon',
    templateUrl: './naic-accordeon.component.html',
    styleUrls: ['./naic-accordeon.component.css'],
    providers: [NgbAccordionConfig] // add the NgbAccordionConfig to the component providers
})

export class NaicAccordeonComponent implements OnInit ,  AfterViewInit {
    @Input() data: any;
    @Output() chartResetzoom = new EventEmitter();
    @Output() chartZoomKW = new EventEmitter();
    @Output() chartZoomPrice = new EventEmitter();
    @Output() chartRedraw = new EventEmitter();
    @ViewChild('panel0') panel0: ElementRef;
    @ViewChild('panel1') panel1: ElementRef;
    @ViewChild('panel2') panel2: ElementRef;
    @ViewChild('panel3') panel3: ElementRef;
    @ViewChild('panel4') panel4: ElementRef;
    kz;
    Naic_KZbuyp = 25;
    Naic_KZholdp = 50;
    Naic_KZsellp = 25;
    berLaagstePrijs: number;
    ingLaagstePrijs: number;
    hlRatio: number;
    actualYield: number;
    expGemWpa: number;
    DivReturn: number;
    expRateReturn: number;
    TotReturn: number;
    changeButton: string;
    manEvalSeries: any;
    activeIds: string[] = [];
    location: string;

    constructor(public user: UserService, private dataService: DataService, private f: FunctionsService, private router: Router,
        public config: NgbAccordionConfig) { }

    ngOnInit() {
        console.log('naic - accordeon onInit');
        if (this.data.enoughdata) { this.showResults(); };

        if (this.data.PeHist) {
            const series = this.data.PeHist.Series;
            for (let i = 0; i < series.length; i++) {
                series[i].HpeOk = true;
                series[i].LpeOk = true;
                series[i].PayOk = true;
            };
        }
        if (this.data.ManEval) {
            if (this.user.isMobile && this.data.ManEval.Series.length > 5) {
                this.manEvalSeries = this.data.ManEval.Series.slice(this.data.ManEval.Series.length - 6, this.data.ManEval.Series.length );
            } else {
                this.manEvalSeries = this.data.ManEval.Series;
            }
        }
        this.changeButton = this.data.savedStudy ? 'Discard your Naic-study' : '';
    }

    ngAfterViewInit() {
        this.closeAllPanels();
        this.panel0.nativeElement.style.display = 'block';
    }

    onChangeButtonClick() {
        if (this.changeButton.substr(0, 7) === 'Discard') {
            this.data.NaicStudy.user_name = this.user.name;
            this.data.NaicStudy.Company = this.data.Company;
            this.data.NaicStudy.oper = 'delete';
        } else { // Save Changes
            this.data.NaicStudy.user_name = this.user.name;
            this.data.NaicStudy.Company = this.data.Company;
            this.data.NaicStudy.oper = 'save';
        }
        this.dataService.updateNaic(this.data.NaicStudy).subscribe(data => {
            this.router.navigate(['company', this.user.getCompanyUrl(this.user.curCompany), 'dummy']);
        });
    }

    panelChange(event: any) {
        event.preventDefault();
        this.closeAllPanels();
        const txt = event.srcElement.innerHTML;
        if (txt.indexOf('1.') >= 0) { this.panel0.nativeElement.style.display = 'block'; }
        if (txt.indexOf('2.') >= 0) { this.panel1.nativeElement.style.display = 'block'; }
        if (txt.indexOf('3.') >= 0) { this.panel2.nativeElement.style.display = 'block'; }
        if (txt.indexOf('4.') >= 0) { this.panel3.nativeElement.style.display = 'block'; }
        if (txt.indexOf('5.') >= 0) { this.panel4.nativeElement.style.display = 'block'; }
        if (txt.indexOf('3.') >= 0) { this.chartZoomKW.emit(null); }
        if (txt.indexOf('4.') >= 0) { this.chartZoomPrice.emit(null); }
      }

    closeAllPanels() {
        this.panel0.nativeElement.style.display = 'none';
        this.panel1.nativeElement.style.display = 'none';
        this.panel2.nativeElement.style.display = 'none';
        this.panel3.nativeElement.style.display = 'none';
        this.panel4.nativeElement.style.display = 'none';
        this.chartResetzoom.emit(true);
    }

    changePar(par: string, index: number) {
        if (par === 'OmzetP') { this.user.curStock.OmzetGroei = this.data.NaicStudy.OmzetGroei; };
        if (par === 'WpaP') { this.user.curStock.WpaGroei = this.data.NaicStudy.WpaGroei; this.recalculate(); };
        if (par === 'Hpe') { this.data.PeHist.Series[index].HpeOk = !this.data.PeHist.Series[index].HpeOk; this.reCalculatePE(); };
        if (par === 'Lpe') { this.data.PeHist.Series[index].LpeOk = !this.data.PeHist.Series[index].LpeOk; this.reCalculatePE(); };
        if (par === 'Pay') { this.data.PeHist.Series[index].PayOk = !this.data.PeHist.Series[index].PayOk; this.reCalculatePE(); };
        if (par === 'HKW') {this.recalculate(); };
        if (par === 'LKW') {this.recalculate(); };
        if (par === 'LPrijs') {};
        if (par === 'PayoutP') {};
        if (par === 'LWpa') {this.recalculate(); };
        this.showResults();
        this.chartRedraw.emit(true);
        this.changeButton = 'Save your Naic-study';
    }

    recalculate() {
        this.data.NaicStudy.HoogsteWpa =
            this.f.futureValue(this.data.NaicStudy.LaagsteWpa, this.data.NaicStudy.WpaGroei, 5);
        this.data.NaicStudy.HoogstePrijs = Math.round(this.data.NaicStudy.HoogsteKW * this.data.NaicStudy.HoogsteWpa * 10) / 10;
        this.berLaagstePrijs = Math.round(this.data.NaicStudy.LaagsteKW * this.data.NaicStudy.LaagsteWpa * 10) / 10;
        this.data.NaicStudy.LaagstePrijs = this.berLaagstePrijs;
    }

    showResults() {
        this.berLaagstePrijs = Math.round(this.data.NaicStudy.LaagsteKW * this.data.NaicStudy.LaagsteWpa * 10) / 10;
        this.kz = this.f.koersZones(
            +this.user.curStock.Value, +this.data.NaicStudy.LaagstePrijs, +this.data.NaicStudy.HoogstePrijs, 25, 50);
        this.user.curStock.Stars = this.f.calculateStars(this.user.curStock);
        this.user.curStock.Advice = this.kz.Advice;
        this.actualYield = this.data.PeHist.LastDividend / this.user.curStock.Value * 100;
        this.expGemWpa = this.f.expGemWpa(+this.data.NaicStudy.LaagsteWpa, +this.data.NaicStudy.WpaGroei, 5);
        this.DivReturn = this.f.expDividend(this.expGemWpa, this.data.NaicStudy.ExpPayout, this.user.curStock.Value);
        this.expRateReturn = (this.data.NaicStudy.HoogstePrijs > 0) ?
            this.f.cagr(this.user.curStock.Value, +this.data.NaicStudy.HoogstePrijs, 5) : null;
        this.TotReturn = (this.data.NaicStudy.HoogstePrijs > 0) ? this.f.round(this.expRateReturn + this.DivReturn, 1) : null;
    }

    reCalculatePE() {
        let TotHpe = 0, TotLpe = 0, TotPay = 0, TotHpeTeller = 0, TotLpeTeller = 0, TotPayTeller = 0;
        const series = this.data.PeHist.Series;
        for (let teller = 0; teller < series.length; teller += 1) {
            if (+series[teller].Hpe > 0 && series[teller].HpeOk) { TotHpe = TotHpe + +series[teller].Hpe; TotHpeTeller += 1; };
            if (+series[teller].Lpe > 0 && series[teller].LpeOk) { TotLpe = TotLpe + +series[teller].Lpe; TotLpeTeller += 1; };
            if (+series[teller].Pay > 0 && series[teller].PayOk) { TotPay = TotPay + +series[teller].Pay; TotPayTeller += 1; };
        }
        this.data.PeHist.AvgHpe = Math.round(TotHpe / TotHpeTeller * 10) / 10;
        this.data.PeHist.AvgLpe = Math.round(TotLpe / TotLpeTeller * 10) / 10;
        this.data.PeHist.AvgPay = Math.round(TotPay / TotPayTeller * 10) / 10;
        this.data.NaicStudy.HoogsteKW = this.data.PeHist.AvgHpe;
        this.data.NaicStudy.LaagsteKW = this.data.PeHist.AvgLpe;
        this.data.NaicStudy.ExpPayout = this.data.PeHist.AvgPay;
        this.recalculate();
    }

    onShowHelp(page) {
        window.open('http://easyshare.basex.be./assets/documentatie/Naicbrochure.pdf#page=' + page)
    }
}
