import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NaicAccordeonComponent } from './naic-accordeon.component';

describe('NaicAccordeonComponent', () => {
  let component: NaicAccordeonComponent;
  let fixture: ComponentFixture<NaicAccordeonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NaicAccordeonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NaicAccordeonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
