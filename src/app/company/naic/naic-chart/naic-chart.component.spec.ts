/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { NaicChartComponent } from './naic-chart.component';

describe('NaicChartComponent', () => {
  let component: NaicChartComponent;
  let fixture: ComponentFixture<NaicChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NaicChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NaicChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
