import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../../../shared/user.service';
import { FunctionsService } from '../../../shared/functions.service';
import * as Highcharts from 'highcharts';
require('highcharts/highcharts-more')(Highcharts);

@Component({
    selector: 'app-naic-chart',
    templateUrl: './naic-chart.component.html',
    styleUrls: ['./naic-chart.component.css']
})
export class NaicChartComponent implements OnInit {
    @Input() naicdata: any;
    @Input() height: any;
    @Input() chartAnimation: any;
    serieData: any;
    Highcharts = Highcharts; // required
    chartCallback = this.saveChart.bind(this);
    options: any;
    chart: any;
    zoomMode = false;

    constructor(private user: UserService, private f: FunctionsService) { }

    ngOnInit() {
        console.log('naic - chart onInit');
        this.fillSeries();
        this.options = {
            title: { text: '' },
            chart: {
                height: this.height,
                borderColor: '#EBBA95',
                borderRadius: 20,
                borderWidth: 2,
                backgroundColor: '#D3E9C3',
                zoomType: 'xy',
            },
            xAxis: { type: 'datetime', minTickInterval: 28 * 24 * 3600 * 1000 },
            yAxis: {
                type: 'logarithmic', minorTickInterval: 1,
                title: { text: '' }, minorGridLineColor: '#cccccc', mmajorGridLineColor: '#cccccc'
            },
            plotOptions: {
                series: { animation: this.chartAnimation,
                          states: {  inactive: { opacity: 1 } } , },
            },
            tooltip: { xDateFormat: '%Y (%Y-%m-%d)', shared: true },
            exporting: { sourceWidth: 400, sourceHeight: 800 },
            series: [
                {
                    name: 'sp10', data: this.serieData.sp10,
                    lineWidth: 1, color: '#A9A9A9 ', enableMouseTracking: false, showInLegend: false,
                    marker: { enabled: false }, allowPointSelect: false
                },
                {
                    name: 'sp20', data: this.serieData.sp20,
                    lineWidth: 1, color: '#A9A9A9 ', enableMouseTracking: false, showInLegend: false,
                    marker: { enabled: false }, allowPointSelect: false
                },
                {
                    name: 'sp30', data: this.serieData.sp30,
                    lineWidth: 1, color: '#A9A9A9 ', enableMouseTracking: false, showInLegend: false,
                    marker: { enabled: false }, allowPointSelect: false
                },
                {
                    name: 'sp40', data: this.serieData.sp40,
                    lineWidth: 1, color: '#A9A9A9 ', enableMouseTracking: false, showInLegend: false,
                    marker: { enabled: false }, allowPointSelect: false
                },
                {
                    name: 'sp50', data: this.serieData.sp50,
                    lineWidth: 1, color: '#A9A9A9 ', enableMouseTracking: false, showInLegend: false,
                    marker: { enabled: false }, allowPointSelect: false
                },
                {
                    name: 'omzet', data: this.serieData.sales,
                    color: '#0000FF', allowPointSelect: true, marker: { symbol: 'circle' }, shadow: true
                },
                {
                    name: 'Winst per aandeel', data: this.serieData.eps,
                    shadow: true, allowPointSelect: true, color: '#008000', marker: { symbol: 'square' },
                    tooltip: { pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.myValue}</b><br/>' }
                },
                {
                    name: 'Verw. Winst per aandeel', data: this.serieData.epsest1,
                    allowPointSelect: true, enableMouseTracking: false, color: '#008000',
                    showInLegend: false, marker: { symbol: 'square' }
                },
                {
                    name: 'Verw. Winst per aandeel', data: this.serieData.epsest2,
                    allowPointSelect: true, enableMouseTracking: false, color: '#008000',
                    showInLegend: false, marker: { symbol: 'square' }
                },
                {
                    name: 'LH - koers', type: 'arearange', data: this.serieData.hl, datalabels: { enabled: false },
                    showInLegend: false, lineWidth: 1, color: '#F58E8E',
                    marker: { enabled: false }
                },
                {
                    name: 'toek. sales', data: this.serieData.bSales, enableMouseTracking: false,
                    showInLegend: false, color: '#0000FF', dashStyle: 'Dash', marker: { enabled: false }, shadow: true
                },
                {
                    name: 'toek. eps', data: this.serieData.bEps, enableMouseTracking: false,
                    showInLegend: false, shadow: true, color: '#008000', dashStyle: 'Dash', marker: { enabled: false },
                },
                {
                    name: 'toek. LH -koers', type: 'arearange', data: this.serieData.bLh, enableMouseTracking: false,
                     datalabels: { enabled: true }, showInLegend: false, lineWidth: 1, color: '#F58E8E', marker: { enabled: false }
                },
                {
                    name: 'koers', data: this.serieData.quotes,
                    enableMouseTracking: false, allowPointSelect: true, shadow: true, color: '#FF0000',
                    lineWidth: 2, marker: { enabled: false }
                },
                {
                    name: 'huidige koers', data: this.serieData.koers, showInLegend: false,
                    color: '#FF0000', marker: { symbol: 'square' }
                },
                {
                    name: 'toekomstige koers', showInLegend: false, dashStyle: 'Dash',
                    enableMouseTracking: false, allowPointSelect: true, shadow: true, color: '#FF0000',
                    lineWidth: 2, marker: { enabled: false }
                },
                {
                    name: 'hoogste prijs', data: this.serieData.Hoogsteprijs, showInLegend: false,
                    color: 'black', marker: { enabled: false }, lineWidth: 1, dashStyle: 'Dot'
                },
            ],
        };
    }

    ChartUpdate() {
        // *************************************************************************
        // Toekomstige Sales
        // ************************************************************************
        const naicStudy = this.naicdata.NaicStudy;
        if (this.chart.series[5].points.length > 0) { // geen toekomst zonder verleden
            const newData = [];
            const factor = (naicStudy.OmzetGroei === 0) ? 1 : 1 + naicStudy.OmzetGroei / 100;
            const lastdate = new Date(this.chart.series[5].points[this.chart.series[5].points.length - 1].x);
            let lastvalue = +(this.chart.series[5].points[this.chart.series[5].points.length - 1].y);
            newData.push({ x: lastdate, y: lastvalue });
            for (let teller = 1; teller <= 5; teller += 1) {
                let newDate = new Date(lastdate); // telkens nieuwe instantie
                newDate = new Date(newDate.setFullYear(newDate.getFullYear() + teller));
                lastvalue = Math.round(lastvalue * factor * 10) / 10;
                newData.push({ x: newDate, y: lastvalue });
            } // einde for
            this.chart.series[10].setData(newData);
        } // einde if
        // *************************************************************************
        // Toekomstige Eps en Low-High
        // ************************************************************************
        if (this.chart.series[6].points.length > 0 && this.chart.series[9].points.length > 0) { // geen toekomst zonder verleden
            const newDataEps = [];
            const newDataLh = [];
            const factor = (naicStudy.WpaGroei === 0) ? 1 : 1 + naicStudy.WpaGroei / 100;
            const lastdate = this.chart.series[6].points[this.chart.series[6].points.length - 1].x;
            let lastvalue = naicStudy.LaagsteWpa; // kan gewijzigd worden
            newDataEps.push({ x: lastdate, y: lastvalue < 0 ? 0.1 : +lastvalue });
            newDataLh.push({
                x: this.chart.series[9].points[this.chart.series[9].points.length - 1].x,
                myValue: lastvalue,
                high: this.chart.series[9].points[this.chart.series[9].points.length - 1].high,
                low: this.chart.series[9].points[this.chart.series[9].points.length - 1].low,
                dataLabels: { enabled: false, formatter: function () { return Math.round(this.y / this.point.myValue * 10) / 10; } }
            });
            for (let teller = 1; teller <= 5; teller += 1) {
                let newDate = new Date(lastdate); // telkens nieuwe instantie
                newDate = new Date(newDate.setFullYear(newDate.getFullYear() + teller));
                lastvalue = lastvalue * factor * 10 / 10;
                if (lastvalue > 0) {
                    newDataEps.push({ x: newDate, y: lastvalue < 0 ? 0.1 : +lastvalue, id: lastvalue });
                    if (naicStudy.HoogsteKW > 0 && naicStudy.LaagsteKW > 0) {
                        newDataLh.push({
                            x: newDate,
                            myValue: lastvalue,
                            high: lastvalue * naicStudy.HoogsteKW,
                            low: lastvalue * naicStudy.LaagsteKW,
                            dataLabels: {
                                enabled: this.zoomMode,
                                formatter: function () { return Math.round(this.y / this.point.myValue * 10) / 10; }
                            }
                        });
                    }
                }
            }
            this.chart.series[11].setData(newDataEps);
            this.chart.series[12].setData(newDataLh);
            // *************************************************************************
            // toekomstige koers - koers oscilleert in toekomstige LH Zone
            // ************************************************************************
            const newDataToekK = [];
            // 1ste punt is huidige koers
            if (this.serieData.koers.length > 0) {
                const point1 = this.serieData.koers[0];
                newDataToekK.push({ x: point1.x, y: point1.y });
                // en nu in de newdatalh
                if (newDataLh.length > 2) {
                    let curpoint = newDataLh[2];
                    let downtrend = false;
                    if (curpoint.high - point1.y > point1.y - curpoint.low) { downtrend = true; }
                    for (let teller = 2; teller < newDataLh.length; teller += 1) {
                        curpoint = newDataLh[teller];
                        downtrend ?
                            newDataToekK.push({ x: curpoint.x, y: curpoint.low })
                            : newDataToekK.push({ x: curpoint.x, y: curpoint.high });
                        downtrend = !downtrend;
                    }
                }
            }
            // this.chart.series[15].setData(newDataToekK);
            // wegggehaald in samenspraak Guido van Herreweghen dd 18/082017
            // *************************************************************************
            // hoogste en laagste prijs
            // ************************************************************************
            const newDataHp = [];
            if (newDataLh.length > 5) {
                newDataHp.push({ x: newDataLh[1].x, y: +naicStudy.HoogstePrijs });
                newDataHp.push({ x: newDataLh[5].x, y: +naicStudy.HoogstePrijs });
                this.chart.series[16].setData(newDataHp);
            }
        }
    }

    saveChart(chart) {
        this.chart = chart;
        console.log('functie aangeroepen !!!!');
        this.ChartUpdate();
    }

    ResetZoom() {
        this.zoomMode = false;
        const lhserie = this.chart.series[9];
        for (let teller = 1; teller < lhserie.points.length; teller += 1) {
            lhserie.points[teller].options.dataLabels.enabled = false;
        }
        const lhserie1 = this.chart.series[12];
        for (let teller = 1; teller < lhserie1.points.length; teller += 1) {
            lhserie1.points[teller].options.dataLabels.enabled = false;
        }
        this.chart.xAxis[0].setExtremes(null, null);
        this.chart.yAxis[0].setExtremes(null, null);
    }

    ZoomKW() {
        this.zoomMode = !this.zoomMode;
        if (!this.zoomMode) {
            this.ResetZoom();
        } else {
            // grenzen vastleggen in functie van series !!!!
            const xaxis = this.chart.xAxis[0];
            const yaxis = this.chart.yAxis[0];
            let lowestEps = 1;
            for (let teller = 0; teller < this.naicdata.epsseries.length; teller += 1) {
                const myValue = this.naicdata.epsseries[teller][1];
                if (myValue < lowestEps && myValue > 0.1) {lowestEps = myValue; }
            }
            let hoogstePrijs = this.naicdata.NaicStudy.HoogstePrijs;
            if (+(this.user.curStock.Value) > +(this.naicdata.NaicStudy.HoogstePrijs)) {hoogstePrijs  = this.user.curStock.Value; }
            if (hoogstePrijs < 10) {hoogstePrijs = 10; }
            xaxis.xAxisMin = Date.UTC(+this.naicdata.LastBookYear - 2, 0, 1);
            xaxis.xAxisMax = Date.UTC(+this.naicdata.LastBookYear + 6, 0, 1);
            yaxis.yAxisMin = lowestEps;
            yaxis.yAxisMax = +hoogstePrijs * 1.2;
            this.chart.xAxis[0].setExtremes(xaxis.xAxisMin, xaxis.xAxisMax);
            this.chart.yAxis[0].setExtremes(yaxis.yAxisMin, yaxis.yAxisMax);
            // show labels
            const lhserie = this.chart.series[9];
            for (let teller = 1; teller < lhserie.points.length; teller += 1) {
                lhserie.points[teller].options.dataLabels.enabled = true;
            }
            const lhserie1 = this.chart.series[12];
            for (let teller = 1; teller < lhserie1.points.length; teller += 1) {
                lhserie1.points[teller].options.dataLabels.enabled = true;
            }
            // enigste manier om labels in orde te krijgen
            this.chart.update({chart: {height: this.height - 1}});
            this.height = this.height - 1;
        }
    }

    onSeriesHide(event) {}

    onPointSelect(event) {}

    fillSeries() {
        // ****************************************************************************
        //    Maak de vaste series klaar
        //    straks moet dit op de server
        // ****************************************************************************
        const curYear = (new Date).getFullYear();
        const minYear = Date.UTC(curYear - 10, 1, 1);
        const maxYear = Date.UTC(curYear + 5, 1, 1);
        this.serieData = {};
        const sd = this.serieData;
        sd.sp10 = [];
        sd.sp10.push({ x: minYear, y: 1 });
        sd.sp10.push({ x: maxYear, y: 1 * Math.pow(1.1, 15), dataLabels: { enabled: true, format: '10 %' } });
        sd.sp20 = [];
        sd.sp20.push({ x: minYear, y: 1 });
        sd.sp20.push({ x: maxYear, y: 1 * Math.pow(1.2, 15), dataLabels: { enabled: true, format: '20 %' } });
        sd.sp30 = [];
        sd.sp30.push({ x: minYear, y: 1 });
        sd.sp30.push({ x: maxYear, y: 1 * Math.pow(1.3, 15), dataLabels: { enabled: true, format: '30 %' } });
        sd.sp40 = [];
        sd.sp40.push({ x: minYear, y: 1 });
        sd.sp40.push({ x: maxYear, y: 1 * Math.pow(1.4, 15), dataLabels: { enabled: true, format: '40 %' } });
        sd.sp50 = [];
        sd.sp50.push({ x: minYear, y: 1 });
        sd.sp50.push({ x: maxYear, y: 1 * Math.pow(1.5, 15), dataLabels: { enabled: true, format: '50 %' } });
        sd.sales = [];
        for (let teller = this.naicdata.salesseries.length - 1; teller >= 0; teller -= 1) {
            sd.sales.push({ x: this.f.dateUtc(this.naicdata.salesseries[teller][0]), y: + this.naicdata.salesseries[teller][1] });
        }
        sd.eps = [];
        for (let teller = this.naicdata.epsseries.length - 1; teller >= 0; teller -= 1) {
            const myValue = this.naicdata.epslabels[this.naicdata.epsseries.length - teller - 1];
            sd.eps.push({
                x: this.f.dateUtc(this.naicdata.epsseries[teller][0]),
                y: this.naicdata.epsseries[teller][1],
                myValue: +myValue,
                dataLabels: { enabled: true, format: myValue },
            });
        };
        sd.epsest1 = [];
        if (this.naicdata.epsest1serie.length > 0) {
            const myValue = this.naicdata.epsest1serie[0][1];
            sd.epsest1.push({
                x: this.f.dateUtc(this.naicdata.epsest1serie[0][0]),
                y: myValue > 0 ? myValue : 0.1, myValue: +myValue, dataLabels: { enabled: true, format: myValue.toString() }
            });
        }
        sd.epsest2 = [];
        if (this.naicdata.epsest2serie.length > 0) {
            const myValue = this.naicdata.epsest2serie[0][1];
            sd.epsest2.push({
                x: this.f.dateUtc(this.naicdata.epsest2serie[0][0]),
                y: myValue > 0 ? myValue : 0.1, myValue: +myValue, dataLabels: { enabled: true, format: myValue.toString() }
            });
        }
        sd.koers = []; // halen we uit curstock
        let dateUtc = 0;
        if (this.user.curStock.RateDate.length < 6) { // we hebben te maken met een tijd - koers van vandaag
            const now = new Date();
            dateUtc = Date.UTC(now.getFullYear(), now.getMonth() , now.getDate());
        } else {
            const date_array = this.user.curStock.RateDate.split('/');
            dateUtc = Date.UTC(+date_array[2] + 2000, +date_array[1] - 1, + date_array[0]);
        }
        sd.koers.push({
            x: dateUtc,
            y: +this.user.curStock.Value, dataLabels: { enabled: true }
        });
        sd.quotes = [];
        for (let teller = this.naicdata.Quotes.length - 1; teller >= 0; teller -= 1) {
            sd.quotes.push({
                x: this.f.dateUtc(this.naicdata.Quotes[teller].Date),
                y: + this.naicdata.Quotes[teller].Value
            });
        }
        sd.hl = [];
        if (this.naicdata.Results && this.naicdata.Results.length > 0) {
            let month = this.naicdata.FiscYearEnd;
            if (month === '') { month = '12'; };
            if (month.length === 1) { month = '0' + month; };
            for (let teller = this.naicdata.Results.length - 1; teller >= 0; teller -= 1) {
                const datum = this.naicdata.Results[teller].Year + '-' + month + '-28';
                if (this.naicdata.Results[teller].HighPrice > 0 && this.naicdata.Results[teller].LowPrice > 0) {
                    sd.hl.push({
                        x: this.f.dateUtc(datum),
                        high: +this.naicdata.Results[teller].HighPrice,
                        low: +this.naicdata.Results[teller].LowPrice,
                        myValue: +this.naicdata.Results[teller].Eps,
                        dataLabels: { enabled: false, formatter: function () { return Math.round(this.y / this.point.myValue * 10) / 10; } }
                    });
                }
            }
        }
    }
}
