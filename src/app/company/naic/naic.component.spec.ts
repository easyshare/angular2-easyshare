/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { NaicComponent } from './naic.component';

describe('NaicComponent', () => {
  let component: NaicComponent;
  let fixture: ComponentFixture<NaicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NaicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NaicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
