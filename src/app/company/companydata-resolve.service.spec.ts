/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CompanyDataResolveService } from './companydata-resolve.service';

describe('NaicResolveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CompanyDataResolveService]
    });
  });

  it('should ...', inject([CompanyDataResolveService], (service: CompanyDataResolveService) => {
    expect(service).toBeTruthy();
  }));
});
