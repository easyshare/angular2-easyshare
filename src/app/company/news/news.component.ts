import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/user.service';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../../shared/data.service';

@Component({
    selector: 'app-news',
    templateUrl: './news.component.html',
    styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
    allNews: any;
    url;
    constructor(private user: UserService, private dataService: DataService, private route: ActivatedRoute) { }

    ngOnInit() {
        this.url = this.route.snapshot.url[0].path;
        if (this.url === 'news') {
            // mobieltje komt via menu news in company navbar en heeft resolve
            this.route.data.forEach((data) => { this.allNews = data.data; this.formatNews(); });
        } else {
            // zal desktop zijn - komt via menu general
            this.dataService.readYahooNews(this.user.curStock.Symbol).subscribe
                (data => { this.allNews = data; this.formatNews(); });
        }
        console.log('News-component onInit via menu ' + this.url );
    }

    formatNews() {
        for (const news of this.allNews) {
            // datumvoorbeeld  "Apr 19th, 11:57am"
            // const parts = news.date.split(',');
            // let time = + parts[1].substr(0, parts[1].length - 2 );
            // const ampm = parts[1].substr(parts[1].length - 2, 2 );
            // const part0 = parts[0].split(' ');
            // const month = part0[0];
            // const day = + part0[1].substr(0, part0[1].length - 2 );
            const href = news.url.match(/<a(.*?)>/)[1];
            // const href = aTag.match( /'(.*?)'/ )[1];
            const titel = news.url.match(/>(.*?)</)[1];
            news.url1 = '';
            news.url1 = news.url1.concat('<a ', href, 'target="_blank"', '>', titel, '</a>');
        }
    }

    onUserSelectRow(news) { }

}
