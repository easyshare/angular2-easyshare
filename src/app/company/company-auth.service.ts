import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from '../shared/user.service';

@Injectable()
export class CompanyAuthService implements CanActivate {

  constructor(private user: UserService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // ****************************************************************
    //    Zet de parameters van de route in orde
    //    omwille van de children in de routing module, kunnen we niet toestaan
    //    dat er een route komt zonder bedrijf
    // ****************************************************************
    let company = route.params['id'];
    if (company === undefined) { // er staat geen bedrijf of view in de url - neem maar het volgend lijntje in de router module
      company = this.user.curCompany;
      this.router.navigate([state.url, this.user.getCompanyUrl(company), this.user.curCompanyView]);
      return false;
    } else {
      this.user.setCompany(company);
      console.log('company set and authorized', );
      return true;
    }
  }
}


