import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { UserService } from '../../shared/user.service';

@Component({
  selector: 'app-dummy',
  template: '',
})
// *********************************************************************************
// http://stackoverflow.com/questions/38971660/angular-2-reload-route-on-param-change
// *********************************************************************************
export class DummyComponent implements OnInit {
   constructor(private _router: Router, private user: UserService) { }

  ngOnInit() {
    console.log('ín dummy component');
    const link: any[] = ['/company', this.user.getCompanyUrl( this.user.curCompany), this.user.curCompanyView];
    this._router.navigate(link);
  }

}
