import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../shared/user.service';
import * as Highcharts from 'highcharts';
require('highcharts/highcharts-more')(Highcharts);

@Component({
    selector: 'app-finance',
    templateUrl: './finance.component.html',
    styleUrls: ['./finance.component.css']
})
export class FinanceComponent implements OnInit {
    data: any;
    Highcharts = Highcharts; // required
    optionsChart1: Object;
    optionsChart2: Object;
    optionsChart3: Object;

    constructor(private user: UserService, private route: ActivatedRoute, ) {
        this.route.data.forEach((data: { data: any[] }) => {
            this.data = data.data;
        });

    }

    ngOnInit() {
        console.log('Finance component oninit');
        this.user.setCompanyView('finance');
        this.initCharts();
    }

    initCharts() {
        console.log('Management-component onInit');
        const serie1 = [];
        const serie1Avg = [];
        const serie2 = [];
        const serie2Avg = [];
        const jaren = [];
        const debtJaren = [];
        const serie3 = [];
        let avg1 = 0;
        let avg2 = 0;

        for (let i = 0; i < this.data.ManEval.Series.length; i++) {
            if (i > this.data.ManEval.Series.length - 7 && i < this.data.ManEval.Series.length - 1) {
                jaren.push(this.data.ManEval.Series[i].Head);
                serie1.push(+this.data.ManEval.Series[i].ProfOnS);
                serie2.push(+this.data.ManEval.Series[i].Roe);
            }
            if (i === this.data.ManEval.Series.length - 1) {
                avg1 = +this.data.ManEval.Series[i].ProfOnS;
                avg2 = +this.data.ManEval.Series[i].Roe;
                for (let j = 0; j < jaren.length; j++) {
                    serie1Avg.push(avg1);
                    serie2Avg.push(avg2);
                }
            }
        }

        this.optionsChart1 = {
            title: { text: '<b>% Pre-tax Profit on Sales</b>' },
            subtitle: { text: '<small><var>Net Before Taxes / Sales</var></small>' },
            margin: [100, 100, 100, 100],
            xAxis: { categories: jaren, crosshair: true },
            series: [
                {
                    type: 'column', name: 'Profit', data: serie1, dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                { type: 'spline', name: 'Last 5 years avg', data: serie1Avg }
            ]
        };

        this.optionsChart2 = {
            title: { text: '<b>% Earned on Equity</b>' },
            subtitle: { text: '<small><var>EPS/ Book Value</var></small>' },
            margin: [100, 100, 100, 100],
            xAxis: { categories: jaren, crosshair: true },
            series: [
                {
                    type: 'column', name: 'ROE', data: serie2, dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                { type: 'spline', name: 'Last 5 years avg', data: serie2Avg }
            ]
        };

        let serie3Leeg = true;
        for (let i = this.data.Results.length - 1; i >= 0; i--) {
            debtJaren.push(this.data.Results[i].Year);
            serie3.push(+this.data.Results[i].Liabilities);
            if (this.data.Results[i].Liabilities > 0) {
                serie3Leeg = false;
            }
        }
        if (!serie3Leeg) {
            this.optionsChart3 = {
                title: { text: '<b>% Debt Ratio</b>' },
                subtitle: { text: '<small><var>vreemd vermogen/balanstotaal</var></small>' },
                xAxis: { categories: debtJaren, crosshair: true },
                 series: [
                {
                    type: 'column', name: 'Debt Ratio', data: serie3, dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
            ]
            };
        }
    }
}
