import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { UserService } from '../shared/user.service';
import { DataService } from '../shared/data.service';
import { Observable } from 'rxjs';

@Injectable()
export class CompanyDataResolveService {

  constructor(private user: UserService, private dataService: DataService, private router: Router) { }
  resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | any {
    return this.dataService.loadnaic(this.user.curCompany, this.user.name);
  }
}
