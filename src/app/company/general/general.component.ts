import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../shared/user.service';
import { DataService } from '../../shared/data.service';
import { RatingComponent} from '../rating/rating.component';
import { FunctionsService } from '../../shared/functions.service';

@Component({
    selector: 'app-general',
    templateUrl: './general.component.html',
    styleUrls: ['./general.component.css']
})
export class GeneralComponent implements OnInit {
    @ViewChild(RatingComponent) ratingForm: RatingComponent;
    data: any;
    newData: any;
    chartHeight = 600;
    ltsteBoekjaar = '';
    datum = '';
    BusinessSize = '';
    kz: any;
    DivReturn = 0.0;
    expRateReturn = 0.0;
    TotReturn = 0.0;
    fourTradersUrl= '';
    reutersUrl= '';
    daysAgo: any;
    showNaicStudy = true;

    constructor(private route: ActivatedRoute, public user: UserService, private dataService: DataService, private f: FunctionsService) {
        this.route.data.forEach((data: { data: any[] }) => {
            this.data = data.data;
            this.f.calculateNaic(this.data);
        });
    }

    ngOnInit() {
        console.log('general-component onInit');
        this.user.setCompanyView('general');
        if (this.data.Results.length > 0) {
            this.ltsteBoekjaar = this.data.Results[0].Year;
            const dd = new Date(+this.ltsteBoekjaar, this.data.FiscYearEnd, 0);
            this.datum = dd.getDate() + '/' + this.data.FiscYearEnd + '/' + this.ltsteBoekjaar;
            this.BusinessSize = this.data.Results[0].Sales;
        }
        if (typeof this.data.NaicStudy !== 'undefined') {
            this.kz = this.f.koersZones(
                +this.user.curStock.Value, +this.data.NaicStudy.LaagstePrijs, +this.data.NaicStudy.HoogstePrijs, 25, 50);
            const expGemWpa = this.f.expGemWpa(+this.data.NaicStudy.LaagsteWpa, +this.data.NaicStudy.WpaGroei, 5);
            this.DivReturn = this.f.expDividend(expGemWpa, this.data.NaicStudy.ExpPayout, this.user.curStock.Value);
            this.expRateReturn = (this.data.NaicStudy.HoogstePrijs > 0) ?
                this.f.cagr(this.user.curStock.Value, +this.data.NaicStudy.HoogstePrijs, 5) : null;
            this.TotReturn = (this.data.NaicStudy.HoogstePrijs > 0) ? this.f.round(this.expRateReturn + this.DivReturn, 1) : null;
        }
        this.showNaicStudy = true;
        if (this.data.Rating < 60) {this.showNaicStudy = false; }
        this.fourTradersUrl = 'http://www.4-traders.com';
        this.fourTradersUrl +=  this.user.curStock['4TradersSymbol'] === '' ?
             '' : '/' + this.user.curStock['4TradersSymbol'] + '/';
        if ( this.user.curStock['ReutersSymbol'] !== '') {
            this.reutersUrl = 'https://www.reuters.com/finance/stocks/analyst/' + this.user.curStock['ReutersSymbol'];
        }
        const ONEDAY = 1000 * 60 * 60 * 24;
        const diff = Date.now() - Date.parse(this.data.LastUpdate);
        this.daysAgo = Math.floor(diff / ONEDAY);
    }

    showRating($event: any) {
        console.log('geklikt');
        this.ratingForm.show(this.data);
    }
}
