/* import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-zoning',
    template: ' <chart [options]="optionsChart1"> </chart>',
    styles: ['chart {display: block;}']
})
export class ZoningComponent implements OnInit {
    optionsChart1: Object;

    constructor() { }

    ngOnInit() {
        this.optionsChart1 = this.defineOptions(42, 25, 50, 100, 125);
    }

    defineOptions(price, low, hold, sell, high) {
        return {
            title: { text: '' },
            xAxis: { categories: [1], visible: false },
            yAxis: { min: 20, title: { text: '' } },
            exporting: { enabled: false },
            plotOptions: {
                column: {
                    stacking: 'normal'
                }
            },
            series: [
                {
                    type: 'columnrange',
                    data: [
                        {
                            x: 1, low: low, high: hold, color: '#D3E9C3', dataLabels:
                            {
                                enabled: true, inside: true, formatter: function () {
                                    return this.y === low ? 'BUY' : '';
                                },
                            }
                        },
                        {
                            x: 1, low: hold, high: sell, color: '#ffff80', dataLabels:
                            {
                                enabled: true, inside: true, formatter: function () {
                                    return this.y === hold ? 'HOLD' : '';
                                },
                            }
                        },
                        {
                            x: 1, low: sell, high: high, color: '#ff704d', dataLabels:
                            {
                                enabled: true, inside: true, formatter: function () {
                                    return this.y === sell ? 'SELL' : '';
                                },
                            }
                        }],
                    pointWidth: 40,
                    borderColor: '#303030',
                    showInLegend: false,
                },
                {
                    name: 'current price',
                    type: 'line',
                    lineWidth: 4,
                    marker: '',
                    data: [{ x: 0.3, y: price }, { x: 1.7, y: price }],
                    showInLegend: false
                },
            ]
        };

    }
} */
