import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../shared/user.service';
import { DataService } from '../../shared/data.service';

@Component({
    selector: 'app-delete',
    templateUrl: './delete.component.html',
    styleUrls: ['./delete.component.css']
})
export class DeleteComponent implements OnInit {
    title = 'Delete Company';
    company = '';

    constructor(private _router: Router, private user: UserService, private data: DataService) { }

    ngOnInit() {
        console.log(this.title + this.user.curCompany);
        this.company = this.user.curCompany;
    }

    onDeleteClick() {
        this.data.updateStocks('del', this.user.curCompany, null)
            .subscribe(data => {
                const link: any[] = ['/company', this.user.getCompanyUrl(this.user.curCompany), this.user.curCompanyView];
                this._router.navigate(link);
            }
            );
    }

}
