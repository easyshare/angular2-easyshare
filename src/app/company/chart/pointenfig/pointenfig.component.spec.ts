import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PointenfigComponent } from './pointenfig.component';

describe('PointenfigComponent', () => {
  let component: PointenfigComponent;
  let fixture: ComponentFixture<PointenfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PointenfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointenfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
