import { Component, ViewChild, Input, OnInit } from '@angular/core';
import { UserService } from '../../../shared/user.service';

@Component({
  selector: 'app-pointenfig',
  templateUrl: './pointenfig.component.html',
  styleUrls: ['./pointenfig.component.css']
})
export class PointenfigComponent implements OnInit {
    @ViewChild('Chartingcanvas') myCanvas: any;
    canvas: any;
    _quotes: any;
    get quotes(): any { return this._quotes; }
    @Input()
        set quotes(quotes: any) {
            if (quotes) {
                this._quotes = quotes;
                this.showPenF(quotes, this.chartOptions);
            }
      }
    chartOptions = { HighLow: false, Bloksize: 'auto', Reversal: 3 };
    curBloksize;
    curReversalv;
    curReversalp;
    lastQuote;

  constructor(private user: UserService) { }

  ngOnInit() {
    console.log('Point and figures-component onInit');
    this.canvas = this.myCanvas.nativeElement;
  }

  PenFrow(koers) {
    if (this.chartOptions.Bloksize === 'auto') {
        if (koers >= 5000) {
            return 445 + Math.floor(Math.round((koers - 5000) / 100 * 1000) / 1000);
        } else if (koers >= 2000) { // 5000 - 2000 = 3000 /50 = 60 !!
            return 385 + Math.floor(Math.round((koers - 2000) / 50 * 1000) / 1000);
        } else if (koers >= 1000) { // 2000 - 1000 = 1000 /20 = 50 !!
            return 335 + Math.floor(Math.round((koers - 1000) / 20 * 1000) / 1000);
        } else if (koers >= 500) { // 1000 - 500 = 500 / 10 = 50 !!
            return 285 + Math.floor(Math.round((koers - 500) / 10 * 1000) / 1000);
        } else if (koers >= 200) { // 500 - 200 = 300 / 5 = 60 !!
            return 225 + Math.floor(Math.round((koers - 200) / 5 * 1000) / 1000);
        } else if (koers >= 100) { // 200 - 100 = 100 / 2 = 50 !!
            return 175 + Math.floor(Math.round((koers - 100) / 2 * 1000) / 1000);
        } else if (koers >= 20) { // 100 - 20 = 80 / 1 = 80 !!
            return 95 + Math.floor(Math.round((koers - 20) / 1 * 1000) / 1000);
        } else if (koers >= 10) { // 20 - 10 = 10 /0.5 = 20 !!
            return 75 + Math.floor(Math.round((koers - 10) / 0.5 * 1000) / 1000);
        } else if (koers >= 5) { // 10 - 5 = 5 / 0.2 = 25 !!
            return 50 + Math.floor(Math.round((koers - 5) / 0.2 * 1000) / 1000);
        } else { // 5 - 0 = 5 / 0.1 = 50 !!
            return Math.floor(Math.round(koers / 0.1 * 1000) / 1000);
        }
    } else {
        return Math.floor(Math.round(koers / +this.chartOptions.Bloksize * 1000) / 1000);
    }
}

PenFValue(row) {
    if (this.chartOptions.Bloksize === 'auto') {
        if (row >= 445) {
            return 5000 + Math.round((row - 445) * 100);
        } else if (row >= 385) {
            return 2000 + Math.round((row - 385) * 50);
        } else if (row >= 335) {
            return 1000 + Math.round((row - 335) * 20);
        } else if (row >= 285) {
            return 500 + Math.round((row - 285) * 10);
        } else if (row >= 225) {
            return 200 + Math.round((row - 225) * 5);
        } else if (row >= 175) {
            return 100 + Math.round((row - 175) * 2);
        } else if (row >= 95) {
            return 20 + Math.round((row - 95) * 1);
        } else if (row >= 75) {
            return 10 + Math.round((row - 75) * 0.5 * 10) / 10;
        } else if (row >= 50) {
            return 5 + Math.round((row - 50) * 0.2 * 10) / 10;
        } else {
            return Math.round(row * 0.1 * 10) / 10;
        }
    } else {

        return Math.round(row * +this.chartOptions.Bloksize * 10) / 10;
    }
}

getColumn(column) {
    const result = { FirstRow: 0, Trend: '', LastRow: 0 };
    result.FirstRow = column.length - 1;
    result.Trend = column[result.FirstRow].sign === 'X' ? 'Bull' : 'Bear';
    result.LastRow = result.FirstRow;
    while (column[result.LastRow] !== undefined) {
        result.LastRow -= 1;
    }
    result.LastRow = result.LastRow < result.FirstRow ? result.LastRow + 1 : result.LastRow;
    return result;
}

PenFCalculation(data, options) {
    const result = {
        PenFcolumns: [],
        PenFyears: [],
        lastRateRow: 0,
        lastRateValue: 0,
        Breakouts: []
    };
    let curYear = 0;
    let curMonth = 0;
    let curTrend = '';
    let curColumn, curColumnp2,
        curKoers, newMonth, newYear, curMax, curRow, teller, i;


    for (teller = 0; teller < data.length; teller += 1) {
        const curDate = new Date(data[teller][0]); // unix timestamp to date
        newYear = false;
        if (curYear !== curDate.getFullYear()) {
            curYear = curDate.getFullYear();
            newYear = true;
        }
        newMonth = false;
        if (curMonth !== curDate.getMonth() + 1) {
            curMonth = curDate.getMonth() + 1;
            newMonth = true;
        }
        if (teller === 0) { // eerste keer
            curKoers = options.HighLow ? data[teller][2] : data[teller][6];
            curRow = this.PenFrow(curKoers);
            curColumn = 0;
            result.PenFcolumns[curColumn] = [];
            result.PenFcolumns[curColumn][curRow] = { sign: 'X' };
            curMax = curRow;
            curTrend = 'Bull';
        }
        // if (curRow < minRow ) { minRow=curRow  }
        // if (curRow > firstRow ) { firstRow=curRow  }
        if (curTrend === 'Bull') {
            curKoers = options.HighLow ? data[teller][2] : data[teller][6];
            curRow = this.PenFrow(curKoers);
            if (curRow > curMax) { // de koers blijft stijgen
                for (i = curMax + 1; i <= curRow; i += 1) {
                    if (result.PenFcolumns[curColumn][i] === undefined) { result.PenFcolumns[curColumn][i] = { sign: 'X' }; }
                }
                curMax = curRow;
            } else { // de koers daalt
                if ((curMax - curRow) >= options.Reversal) { // we krijgen een trendwissel
                    curColumn += 1;
                    result.PenFcolumns[curColumn] = [];
                    curTrend = 'Bear';
                    for (i = curMax - 1; i >= curRow; i -= 1) {
                        if (result.PenFcolumns[curColumn][i] === undefined) { result.PenFcolumns[curColumn][i] = { sign: 'O' }; }
                    }
                    curMax = curRow;
                } else { // een beetje gedaald in een opgaande trend
                    for (i = curMax - 1; i >= curRow; i -= 1) {
                        if (result.PenFcolumns[curColumn][i] === undefined) { result.PenFcolumns[curColumn][i] = { sign: 'X' }; }
                    }
                }
            }

        }
        if (curTrend === 'Bear') {
            curKoers = options.HighLow ? data[teller][3] : data[teller][6];
            curRow = this.PenFrow(curKoers);
            if (curRow < curMax) { // de koers blijft dalen
                for (i = curMax - 1; i >= curRow; i -= 1) {
                    if (result.PenFcolumns[curColumn][i] === undefined) { result.PenFcolumns[curColumn][i] = { sign: 'O' }; }
                }
                curMax = curRow;
            } else { // de koers stijgt
                if ((curRow - curMax) >= options.Reversal) { // we krijgen een trendwissel
                    curColumn += 1;
                    result.PenFcolumns[curColumn] = [];
                    curTrend = 'Bull';
                    for (i = curMax + 1; i <= curRow; i += 1) {
                        if (result.PenFcolumns[curColumn][i] === undefined) { result.PenFcolumns[curColumn][i] = { sign: 'X' }; }
                    }
                    curMax = curRow;
                } else { // een beetje gestegen in een dalende trend
                    for (i = curMax + 1; i <= curRow; i += 1) {
                        if (result.PenFcolumns[curColumn][i] === undefined) { result.PenFcolumns[curColumn][i] = { sign: 'O' }; }
                    }
                }
            }

        }
        if (newMonth) {
            if (+curMonth === 12) {
                result.PenFcolumns[curColumn][curRow].month = 'C';
            } else {
                if (+curMonth === 11) {
                    result.PenFcolumns[curColumn][curRow].month = 'B';
                } else {
                    if (+curMonth === 10) {
                        result.PenFcolumns[curColumn][curRow].month = 'A';
                    } else {
                        result.PenFcolumns[curColumn][curRow].month = curMonth;
                    }
                }
            }
        }
        if (teller === data.length - 1) { // We hebben de laatste koers te pakken
            result.lastRateRow = curRow;
            result.lastRateValue = curKoers;
        }
        if (newYear) {
            result.PenFyears[curColumn] = curYear;
        }
    }
    // ***************************************************************************************************************
    // **** Bepalen van de price objective column
    // ***************************************************************************************************************
    for (teller = result.PenFcolumns.length - 3; teller >= 0; teller -= 1) {
        curColumn = this.getColumn(result.PenFcolumns[teller]);
        curColumnp2 = this.getColumn(result.PenFcolumns[teller + 2]);
        if (curColumn.Trend === 'Bear') {
            if (curColumnp2.LastRow < curColumn.LastRow) { // Basic Double Bottom Breakdown
                result.Breakouts.push({ Trend: 'down', Column: teller + 2, Row: curColumn.LastRow });
            }

        }
        if (curColumn.Trend === 'Bull') {
            if (curColumn.FirstRow < curColumnp2.FirstRow) { // Basic Double Top Breakouts
                result.Breakouts.push({ Trend: 'up', Column: teller + 2, Row: curColumn.FirstRow });
            }

        }
    }
    return result;
}

drawPenF(canvas, PenFdata) {
    const series = [];
    const blokpixels = 12;
    const yaxisWidth = 75;
    const y2axisWidth = 75;
    const titleWidth = 0;
    const xaxisWidth = 75;
    let numberOfRows = 40, // hier proberen we rond te blijven
        maxColumns = 50, // we laten alleen de 50 laatste trends zien - kan eventueel uitgebreid
        firstcolumn, lastcolumn, curColumn, curRow,
        points,
        text, textDimensions,
        ctx, width, height,
        firstRow = 0, minRow = 999,
        aantalkolommen = 0, teller;

    series[0] = []; // de echte x en o s
    series[1] = []; // de maanden van 1 - 9,a,b,c in het rood

    // ****************************************************************************************************
    // ******          Bepaal het aantal zichtbare kolommen en het nummer van de eerste en laatste kolom
    // ****************************************************************************************************
    aantalkolommen = PenFdata.PenFcolumns.length;
    if (aantalkolommen > maxColumns) {
        firstcolumn = aantalkolommen - 1 - maxColumns;
        lastcolumn = aantalkolommen - 1;
    } else {
        firstcolumn = 0;
        lastcolumn = aantalkolommen - 1;
    }
    // ****************************************************************************************************
    // ******          Bepaal het aantal zichtbare rijen en het nummer van de eerste en laatste rij
    // ******          Vul tevens de series in zwart en deze in rood
    // ****************************************************************************************************
    for (curColumn = firstcolumn; curColumn <= lastcolumn; curColumn += 1) {
        points = PenFdata.PenFcolumns[curColumn];
        teller = points.length - 1;
        while (points[teller] !== undefined) {
            if (teller > firstRow) { firstRow = teller; }
            if (teller < minRow) { minRow = teller; }
            if (points[teller].month !== undefined) {
                series[1].push([curColumn + .4, teller + .5, points[teller].month]);
            } else {
                series[0].push([curColumn + .4, teller + .5, points[teller].sign]);
            }
            teller -= 1;
        }
    }
    firstRow += 2; // een blanko rijtje aan de bovenkant
    maxColumns += 2; // een blanco aan de zijkant
    numberOfRows = firstRow > 2 ? firstRow - minRow + 1 : numberOfRows;

    // **************************************************************************************************************
    // ****           Alles is gekend - we kunnen er aan beginnen
    // **************************************************************************************************************
    width = maxColumns * blokpixels;
    height = numberOfRows * blokpixels;


    canvas.width = yaxisWidth + width + y2axisWidth;
    canvas.height = titleWidth + height + xaxisWidth;

    ctx = canvas.getContext('2d');

    ctx.lineWidth = 1;
    ctx.strokeStyle = '#FF0000';
    ctx.strokeRect(yaxisWidth, titleWidth, width, height);
    ctx.fillStyle = 'lightblue';
    ctx.fillRect(yaxisWidth, titleWidth, width, height);
    ctx.lineWidth = 0.1;
    ctx.strokeStyle = 'black';
    ctx.font = 'Bold Italic 8pt Arial';
    ctx.beginPath();
    for (curColumn = 1; curColumn <= maxColumns; curColumn += 1) {
        ctx.moveTo(yaxisWidth + curColumn * blokpixels, titleWidth);
        ctx.lineTo(yaxisWidth + curColumn * blokpixels, titleWidth + height + 5); // + 5 voor marker
        if (PenFdata.PenFyears[firstcolumn + curColumn] !== undefined) {
            text = PenFdata.PenFyears[firstcolumn + curColumn] + '';
            ctx.fillText(text.substr(2, 2), yaxisWidth + (curColumn * blokpixels), titleWidth + height + 15);
        }
    }
    for (curRow = 1; curRow <= numberOfRows; curRow += 1) {
        ctx.moveTo(yaxisWidth - 5, titleWidth + curRow * blokpixels);
        ctx.lineTo(yaxisWidth + width + 5, titleWidth + curRow * blokpixels); // + 5 voor marker
        if (firstRow - curRow === PenFdata.lastRateRow) {
            text = '<<' + PenFdata.lastRateValue;
            ctx.fillStyle = 'Red';
        } else {
            text = this.PenFValue(firstRow - curRow).toFixed(2);
            ctx.fillStyle = 'Black';
        }
        textDimensions = ctx.measureText(text);
        ctx.fillText(text, yaxisWidth - (textDimensions.width + 5), titleWidth + curRow * blokpixels - 1);
        ctx.fillText(text, yaxisWidth + width + 5, titleWidth + curRow * blokpixels - 1);
    }
    ctx.stroke();
    ctx.font = '10pt Arial';
    ctx.fillStyle = 'black';
    for (teller = 0; teller < series[0].length; teller += 1) {
        curColumn = series[0][teller][0] - 0.4 - firstcolumn;
        curRow = firstRow - (series[0][teller][1] - 0.5);
        ctx.fillText(series[0][teller][2], yaxisWidth + curColumn * blokpixels + 1, titleWidth + curRow * blokpixels - 1);
    }
    ctx.fillStyle = 'red';
    for (teller = 0; teller < series[1].length; teller += 1) {
        curColumn = series[1][teller][0] - 0.4 - firstcolumn;
        curRow = firstRow - (series[1][teller][1] - 0.5);
        ctx.fillText(series[1][teller][2], yaxisWidth + curColumn * blokpixels + 1, titleWidth + curRow * blokpixels - 1);
    }
    // **** Teken de breakouts

    ctx.lineWidth = '0.5';
    for (teller = 0; teller < PenFdata.Breakouts.length; teller += 1) {
        curColumn = PenFdata.Breakouts[teller].Column - firstcolumn;
        if (curColumn > 0) { // de breakout valt op het scherm
            ctx.beginPath();
            curRow = firstRow - PenFdata.Breakouts[teller].Row;
            if (PenFdata.Breakouts[teller].Trend === 'down') {
                ctx.strokeStyle = 'red';
                ctx.moveTo(yaxisWidth + (curColumn - 1.5) * blokpixels, titleWidth + curRow * blokpixels);
                ctx.lineTo(yaxisWidth + (curColumn + 0.5) * blokpixels, titleWidth + curRow * blokpixels);
            } else {
                ctx.strokeStyle = 'green';
                ctx.moveTo(yaxisWidth + (curColumn - 1.5) * blokpixels, titleWidth + (curRow - 1) * blokpixels);
                ctx.lineTo(yaxisWidth + (curColumn + 0.5) * blokpixels, titleWidth + (curRow - 1) * blokpixels);
            }
            ctx.stroke();
        }
    }

}

showPenF(data, options) {
    if (data.length > 0) {
        // Data is gerangschikt van vroeger naar nu
        const lastDate = new Date(data[data.length - 1][0]);
        const today = new Date();
        const curValue = +this.user.curStock.Value;
        const row = this.PenFrow(curValue);
        let yahooDatum: any;

        this.curBloksize = Math.round((this.PenFValue(row) - this.PenFValue(row - 1)) * 10) / 10;
        this.curReversalv = Math.round(this.curBloksize * options.Reversal * 10) / 10;
        this.curReversalp = Math.round(this.curReversalv / curValue * 100 * 10) / 10;
        if (data.length > 0) { // dan zetten we in ieder geval de laatste quote neer
            this.lastQuote = {
                datum: lastDate,
                O: data[data.length - 1][1],
                H: data[data.length - 1][2],
                L: data[data.length - 1][3],
                C: data[data.length - 1][4],
                V: data[data.length - 1][5]
            };
        }
        if (data.length > 0 && (+today - +lastDate) < 7 * 24 * 60 * 60 * 1000) {   // geen 7 dagen oud
            // we voegen de huidige koers toe!
            yahooDatum = new Date(this.user.curStock.LastHistDate);
            data.push([yahooDatum.getTime(), curValue, curValue, curValue, curValue, 0, curValue]);
        } else {
            data = [];
        }
        const PenFdata = this.PenFCalculation(data, options);
        this.drawPenF(this.canvas, PenFdata);
    } else {
        const ctx = this.canvas.getContext('2d');
        // ctx.font = '15px';
        ctx.textAlign = 'center';
        ctx.fillText('No Data', this.canvas.width / 2, this.canvas.height / 2);
    }
}

onChange(id, waarde) {
    if (id === 'ChartselectReversal') { this.chartOptions.Reversal = waarde; }
    if (id === 'ChartselectBlocksize') { this.chartOptions.Bloksize = waarde; }
    if (id === 'ChartselectRates') { waarde === 'High/Low' ? this.chartOptions.HighLow = true : this.chartOptions.HighLow = false; }
    this.showPenF(this.quotes, this.chartOptions);
}


}
