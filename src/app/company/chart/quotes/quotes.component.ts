import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../../../shared/user.service';
import * as Highcharts from 'highcharts';
require('highcharts/highcharts-more')(Highcharts);

@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.css']
})
export class QuotesComponent implements OnInit {
    _quotes: any;
    get quotes(): any { return this._quotes; }
    @Input()
        set quotes(quotes: any) {
            if (quotes) {
                this._quotes = quotes;
                this.update();
            }
      }
    curDate = new Date();
    chartPeriods = [
        { Name: 'YTD' , Value : new Date().setMonth(0 , 1 ) },
        { Name: '1 month', Value: new Date().setMonth(this.curDate.getMonth() - 1) },
        { Name: '3 month', Value: new Date().setMonth(this.curDate.getMonth() - 3) },
        { Name: '6 month', Value: new Date().setMonth(this.curDate.getMonth() - 6) },
        { Name: '1 year', Value: new Date().setMonth(this.curDate.getMonth() - 12) },
        { Name: '2 year', Value: new Date().setMonth(this.curDate.getMonth() - 24) },
        { Name: 'all', Value: null }
    ];
    selectedPeriod = this.chartPeriods[4].Name;
    sma200value: number;
    sma50value: number;
    sma20value: number;
    Highcharts = Highcharts; // required
    chartCallback = this.saveChart.bind(this);
    chart: any;
    seriedata = [];
    options = {
        chart: { height: (11 / 16 * 100) + '%' },
        width: '100%',
        title: { text: undefined },
        rangeSelector: {selected: 1},
        xAxis: { type: 'datetime', minTickInterval: 28 * 24 * 3600 * 1000, labels: {
            format: '{value:%Y-%m-%d}',
            rotation: 45,
            align: 'left'
        } },
        yAxis: {
            title: false
        },
        series: [{ name: 'Rate', tooltip: {valueDecimals: 2} },
                 { name: 'SMA 200', type: 'spline', tooltip: {valueDecimals: 2} },
                 { name: 'SMA 50', type: 'spline', tooltip: {valueDecimals: 2} },
                 { name: 'SMA 20', type: 'spline', tooltip: {valueDecimals: 2} }]
        };

    constructor(private user: UserService) { }

    ngOnInit() {
        console.log('Chart-component onInit');

    }

    onPeriodClick(type: any) {
        this.selectedPeriod = type.Name;
        this.chart.xAxis[0].setExtremes(type.Value, null);
    }

    saveChart(chart: any) {
        this.chart = chart;
    }

    update() {
        this.seriedata = [];
        let aantal = 999;
        // turbotreshold van highcharts = 1000 !
        if  (this.quotes.length < aantal) {aantal = this.quotes.length;}
        for (let teller =  this.quotes.length - aantal; teller < this.quotes.length; teller += 1) {
            if (this.quotes[teller][6] > 0) {
                this.seriedata.push({ x: this.quotes[teller][0] , y: this.quotes[teller][6]});
            }
        }
        this.chart.series[0].name = this.user.curStock.Company;
        this.chart.series[0].setData(this.seriedata);
        this.chart.series[1].setData(this.SMA(200, this.seriedata));
        this.chart.series[2].setData(this.SMA(50, this.seriedata));
        this.chart.series[3].setData(this.SMA(20, this.seriedata));
        if (this.chart.series[1].data.length > 0) {
            this.sma200value = Math.round(this.chart.series[1].data[ this.chart.series[1].data.length - 1 ].y * 100) / 100 ;
        }
        if (this.chart.series[2].data.length > 0) {
            this.sma50value = Math.round(this.chart.series[2].data[ this.chart.series[2].data.length - 1 ].y * 100) / 100 ;
        }
        if (this.chart.series[3].data.length > 0) {
            this.sma20value = Math.round(this.chart.series[3].data[ this.chart.series[3].data.length - 1 ].y * 100) / 100 ;
        }
         // Stel default 1 jaar in.
         this.chart.xAxis[0].setExtremes(this.chartPeriods[4].Value, null);
    }

    SMA(period: number, seriedata: any) {
        let avg = 0;
        const smaData = [];
        for (let teller = 0 ; teller < seriedata.length; teller += 1) {
           avg += seriedata[teller].y;
            if (teller >= period) {
                avg -= seriedata[teller - period].y;
                smaData.push([seriedata[teller].x, avg / period]);
            }
        }
        return smaData;
    }
}
