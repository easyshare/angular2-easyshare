import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../shared/user.service';
import { DataService } from '../../shared/data.service';
import { map } from 'rxjs/operators';

@Component({
    selector: 'app-chart',
    templateUrl: './chart.component.html',
    styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {
    chartTypes = [
        { Name: 'Classical' },
        { Name: 'Points and Figures' }
    ];
    selectedType = this.chartTypes[0].Name;
    quotes: any;

    constructor(private user: UserService, private dataService: DataService) { }

    ngOnInit() {
        console.log('Chart-component onInit');
        this.user.setCompanyView('chart');
        this.dataService.readYahooQuotes(this.user.curStock.Symbol).subscribe(data => {this.quotes = data; });
    }

    onTypeClick(type: any) {
        this.selectedType = type.Name;
    }
}
