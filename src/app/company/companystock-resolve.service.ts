import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { UserService } from '../shared/user.service';
import { DataService } from '../shared/data.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class CompanyStockResolveService implements Resolve<any> {

    constructor(private user: UserService, private dataService: DataService, private router: Router) { }

    resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | any {
        console.log('company resolve - ' + this.user.curCompany + ' looking for data on server')
        return this.dataService.loadstocklist('0', this.user.curCompany, this.user.name).pipe(map(res => res[0]));
    }
}
