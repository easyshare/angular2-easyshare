import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CompanyRoutingModule } from './company-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../shared/shared.module';
import { HighchartsChartModule } from 'highcharts-angular';

import { CompanyAuthService } from './company-auth.service';
import { CompanyStockResolveService } from './companystock-resolve.service';
import { CompanyDataResolveService } from './companydata-resolve.service';
import { NewsResolveService } from './news/news-resolve.service';

import { CompanyComponent } from './company.component';
import { NaicComponent } from './naic/naic.component';
import { NaicAccordeonComponent } from './naic/naic-accordeon/naic-accordeon.component';
import { NaicChartComponent } from './naic/naic-chart/naic-chart.component';
import { FinanceComponent } from './finance/finance.component';
import { NewsComponent } from './news/news.component';
import { GeneralComponent } from './general/general.component';
import { ChartComponent } from './chart/chart.component';
import { DeleteComponent } from './delete/delete.component';
import { QuotesComponent } from './chart/quotes/quotes.component';
import { PointenfigComponent } from './chart/pointenfig/pointenfig.component';
import { RatingComponent } from './rating/rating.component';

@NgModule({
  imports: [
    CommonModule,
    CompanyRoutingModule,
    FormsModule,
    NgbModule.forRoot(),
    HighchartsChartModule,
    SharedModule
  ],
  declarations: [
    CompanyComponent,
    NaicComponent,
    FinanceComponent,
    NaicChartComponent,
    NaicAccordeonComponent,
    GeneralComponent,
    NewsComponent,
    ChartComponent,
    DeleteComponent,
    QuotesComponent,
    PointenfigComponent,
    RatingComponent
  ],
  providers: [
    CompanyStockResolveService,
    CompanyAuthService,
    CompanyDataResolveService,
    NewsResolveService
  ],
})
export class CompanyModule { }
