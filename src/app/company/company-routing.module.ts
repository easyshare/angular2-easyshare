import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompanyComponent } from '../company/company.component';
import { CompanyAuthService } from '../company/company-auth.service';
import { CompanyStockResolveService } from '../company/companystock-resolve.service';
import { NaicComponent } from '../company/naic/naic.component';
import { CompanyDataResolveService } from '../company/companydata-resolve.service';
import { NewsResolveService } from '../company/news/news-resolve.service';
import { FinanceComponent } from '../company/finance/finance.component';
import { DeleteComponent } from '../company/delete/delete.component';
import { GeneralComponent } from '../company/general/general.component';
import { NewsComponent } from '../company/news/news.component';
import { ChartComponent } from './chart/chart.component';
import { AppAuthService } from '../shared/app-auth.service';
import { DummyComponent } from '../company/dummy/dummy.component';

const routes: Routes = [
  { path: 'company', component: CompanyComponent, canActivate: [AppAuthService, CompanyAuthService], },
  { path: 'company/:id', component: CompanyComponent, canActivate: [AppAuthService, CompanyAuthService],
    resolve: { stock: CompanyStockResolveService },
    children: [
      { path: '', redirectTo: 'general', pathMatch: 'full' },
      { path: 'naic', component: NaicComponent, resolve: { data: CompanyDataResolveService } },
      { path: 'finance', component: FinanceComponent, resolve: { data: CompanyDataResolveService } },
      { path: 'general', component: GeneralComponent, resolve: { data: CompanyDataResolveService } },
      { path: 'delete', component: DeleteComponent },
      { path: 'chart', component: ChartComponent },
      { path: 'news', component: NewsComponent , resolve: { data: NewsResolveService }},
      { path: 'dummy', component: DummyComponent }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class CompanyRoutingModule { }
