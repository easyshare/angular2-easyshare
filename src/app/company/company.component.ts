import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../shared/user.service'; // provider in shared.module
import { DataService } from '../shared/data.service'; // provider in shared.module
import { ActivatedRoute, Router } from '@angular/router';
import { FourTradersModalComponent } from '../shared/fourtraders-modal/fourtraders-modal.component';

@Component({
    selector: 'app-company',
    templateUrl: './company.component.html',
    styleUrls: ['./company.component.css']
})

export class CompanyComponent implements OnInit {

    @ViewChild(FourTradersModalComponent) fourTraders: FourTradersModalComponent;
    fourTradersUrl= '';
    fourTrData: string[] = [];

    constructor(public user: UserService, private data: DataService,
         private route: ActivatedRoute, private router: Router) {
    }

    ngOnInit() {
        this.route.data.forEach((data: { stock: any[] }) => { this.user.curStock = data.stock; });
        console.log('company - component onInit');
        if ( this.user.curStock == null) {
            this.router.navigate(['/companynotfound']);
        } else {
            this.fourTradersUrl = 'http://www.4-traders.com';
            this.fourTradersUrl +=  this.user.curStock['4TradersSymbol'] === '' ?
                '' : '/' + this.user.curStock['4TradersSymbol'] + '/';
        }
    }

    volgend() {
        const volgendBedrijf = this.user.getNextCompany();
        this.user.setCompany(volgendBedrijf);
        this.router.navigate(['/company', this.user.getCompanyUrl(volgendBedrijf), 'dummy']);
    }

    vorig() {
        const volgendBedrijf = this.user.getPrevCompany();
        this.user.setCompany(volgendBedrijf);
        this.router.navigate(['/company', this.user.getCompanyUrl(volgendBedrijf), 'dummy']);
    }

    onCompanyClick() {
        this.fourTraders.open(this.user.curStock);
    }

}
