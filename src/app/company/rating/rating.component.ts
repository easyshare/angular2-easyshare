import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css']
})
export class RatingComponent implements OnInit {
    data: any;
    ratingFormVisible = false;
    showContent1: boolean;
    showContent2: boolean;
    showContent3: boolean;
    showContent4: boolean;
    showContent5: boolean;
    showContent6: boolean;
    showContent7: boolean;
    showContent8: boolean;
    showContent9: boolean;
    showContent10: boolean;
    showContent11: boolean;

    constructor() {
    }

    ngOnInit() {
    }

    show(data: any) {
        console.log('in rating show');
        this.data = data;
        this.ratingFormVisible = true;
        this.showContent1 = false;
        this.showContent2 = false;
        this.showContent3 = false;
        this.showContent4 = false;
        this.showContent5 = false;
        this.showContent6 = false;
        this.showContent7 = false;
        this.showContent8 = false;
        this.showContent9 = false;
        this.showContent10 = false;
        this.showContent11 = false;
    }

    onSubmit() {
        this.ratingFormVisible = false;
    }

    showContent(nr: number){
        switch (nr) {
            case 1: {this.showContent1 = !this.showContent1; break; }
            case 2: {this.showContent2 = !this.showContent2; break; }
            case 3: {this.showContent3 = !this.showContent3; break; }
            case 3: {this.showContent3 = !this.showContent3; break; }
            case 4: {this.showContent4 = !this.showContent4; break; }
            case 5: {this.showContent5 = !this.showContent5; break; }
            case 6: {this.showContent6 = !this.showContent6; break; }
            case 7: {this.showContent7 = !this.showContent7; break; }
            case 8: {this.showContent8 = !this.showContent8; break; }
            case 9: {this.showContent9 = !this.showContent9; break; }
            case 10: {this.showContent10 = !this.showContent10; break; }
            case 11: {this.showContent11 = !this.showContent11; break; }
        }

    }

}
