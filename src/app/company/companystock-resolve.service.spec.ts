/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CompanyStockResolveService } from './companystock-resolve.service';

describe('CompanyResolveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CompanyStockResolveService]
    });
  });

  it('should ...', inject([CompanyStockResolveService], (service: CompanyStockResolveService) => {
    expect(service).toBeTruthy();
  }));
});
